

/* Purchase Now button fadein effect begins */
$(document).ready(function () {
	$('.thickbox').colorbox({onComplete : function() { 
                  $(this).colorbox.resize(); 
  }
});
	
	//Append a div with hover class to all the LI
	$('#purchase-now li').append('<div class="hover"><\/div>');

	$('#purchase-now li').hover(
		
		//Mouseover, fadeIn the hidden hover class	
		function() {
			
			$(this).children('div').fadeIn('1000');	
		
		}, 
	
		//Mouseout, fadeOut the hover class
		function() {
		
			$(this).children('div').fadeOut('1000');	
		
	}).click (function () {
	
		//Add selected class if user clicked on it
		$(this).addClass('selected');
		
	});

});
/* Purchase Now button fadein effect ends */