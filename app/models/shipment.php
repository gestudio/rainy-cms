<?php
class Shipment extends AppModel {
	var $name = 'Shipment';
	
	var $belongsTo = array('ShipmentMethod','ShipmentState');
	var $hasMany = array('ShipmentPackage');
	var $validate = array(
		'id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'shipment_method_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'shipment_state_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nombre_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'dni_r' => array(
			'minlength' => array(
				'rule' => array('minlength',9),
				'message' => 'Al menos 9 carácteres',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'direccion_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'direccion_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cp_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cp_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'provincia_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pais_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'provincia_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pais_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nombre_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxlength' => array(
				'rule' => array('maxlength',64),
				'message' => 'Demasiado largo',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ciudad_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ciudad_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'telefono_r' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'telefono_d' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	
	
	
	

	function beforeValidate(){
		parent::beforeValidate();

		// Just sanitizes input 
		// --------------------------------------------------
		// Check all received params for correction such as:
		// $string = ucwords(strtolower(trim($string));
		if (!empty($this->data['Shipment'])) {
			if(!empty($this->data['Shipment']['nombre_r']))	$this->data['Shipment']['nombre_r'] = ucwords(mb_strtolower(trim($this->data['Shipment']['nombre_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['dni_r']))	$this->data['Shipment']['dni_r'] = strtoupper(mb_strtolower(trim($this->data['Shipment']['dni_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['direccion_r']))	$this->data['Shipment']['direccion_r'] = ucwords(mb_strtolower(trim($this->data['Shipment']['direccion_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['ciudad_r']))	$this->data['Shipment']['ciudad_r'] = ucwords(mb_strtolower(trim($this->data['Shipment']['ciudad_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['cp_r']))	$this->data['Shipment']['cp_r'] = strtoupper(mb_strtolower(trim($this->data['Shipment']['cp_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['provincia_r']))	$this->data['Shipment']['provincia_r'] = ucwords(mb_strtolower(trim($this->data['Shipment']['provincia_r']),'UTF-8'));
			if(!empty($this->data['Shipment']['pais_r']))	$this->data['Shipment']['pais_r'] = ucwords(mb_strtolower(trim($this->data['Shipment']['pais_r']),'UTF-8'));
			
			if(!empty($this->data['Shipment']['nombre_d']))	$this->data['Shipment']['nombre_d'] = ucwords(mb_strtolower(trim($this->data['Shipment']['nombre_d']),'UTF-8'));
			if(!empty($this->data['Shipment']['direccion_d']))	$this->data['Shipment']['direccion_d'] = ucwords(mb_strtolower(trim($this->data['Shipment']['direccion_d']),'UTF-8'));
			if(!empty($this->data['Shipment']['ciudad_d']))	$this->data['Shipment']['ciudad_d'] = ucwords(mb_strtolower(trim($this->data['Shipment']['ciudad_d']),'UTF-8'));
			if(!empty($this->data['Shipment']['cp_d']))	$this->data['Shipment']['cp_d'] = strtoupper(mb_strtolower(trim($this->data['Shipment']['cp_d']),'UTF-8'));
			if(!empty($this->data['Shipment']['provincia_d']))	$this->data['Shipment']['provincia_d'] = ucwords(mb_strtolower(trim($this->data['Shipment']['provincia_d']),'UTF-8'));
			if(!empty($this->data['Shipment']['pais_d']))	$this->data['Shipment']['pais_d'] = ucwords(mb_strtolower(trim($this->data['Shipment']['pais_d']),'UTF-8'));
		} 
	}
	
	
	
	
}
