<?
class Contact extends AppModel {
    var $useTable = false;
    var $_schema = array(
        'name'		=>array('type'=>'string', 'length'=>100), 
        'email'		=>array('type'=>'string', 'length'=>255), 
        'details'	=>array('type'=>'text')
    );
    var $validate = array(
                          'name' => array(
                              'rule'=>array('minLength', 1),
                              'message' => 'Campo obligatorio' 
                              ),
                          'phone' => array(
                              'rule'=>'numeric',
                              'message' => 'Debería ser un número de teléfono'
                              ),
                          'email' => array(
                              'rule'=>'email',
                              'message' => 'Debe ser una dirección de correo electrónico'
                              ),
                          'details' => array(
                              'rule'=>array('minLength', 1),
                              'message'=>'Campo obligatorio'
                              )
                        );
}
?>