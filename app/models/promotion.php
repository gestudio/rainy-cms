<?php
class Promotion extends AppModel {
  var $name = 'Promotion';
  var $helpers = array('Cache');
   var $cacheAction = array(
    'index' => array('callbacks' => true, 'duration' => 21600)
  );

  var $validate = array(
    'name'  => array('vacio'  => array('rule' => 'notEmpty', 'message' => 'Campo obligatorio')),
    'title' => array('vacio'  => array('rule' => 'notEmpty', 'message' => 'Campo obligatorio'))
  );


  function set_selected($id, $value = 1) {
    if ($id) {
      $this->id = $id;
      $this->saveField('selected', $value);
    }
  }
  
  
  
  
  
  /*
   * Check if the POST send form included any images to save on the file system.
   */
  function afterSave(){
    
    if (isset($this->data['Promotion']['id'])){
                
              //SET THE CURRENT PROMOTION ID FOR THE UPDATE
              $this->set('id', $this->data['Promotion']['id']);
              
              
              
              
              
              if (!empty($this->data['Promotion']['fichero'])){
                  //UPLOAD THE IMAGE FILE
                  //set the extension depending on the mimetime
                  switch($this->data['Promotion']['fichero']['type']){
                      case 'image/png': $ext = 'png'; break;
                      case 'image/jpeg': $ext = 'jpg'; break;
                      case 'image/pjpeg': $ext = 'jpg'; break;
                      case 'image/gif': $ext = 'gif'; break;
                  }
                  
                  //set the name with the record id and the extension
                  $fileName = str_pad($this->data['Promotion']['id'],10,'0',STR_PAD_LEFT).'.'.$ext; // Creamos el nombre del archivo
                  
                  //save the image in the destination folder with the selected name
                  $action = copyImage($this->data['Promotion']['fichero']['tmp_name'],IMAGES.'promotions'.DS.$fileName,
                                      array(
                                            'width'=>'940',
                                            'height'=> '360',
                                            'quality' => 90,
                                            'print' => false,
                                            'type' => 'image/jpeg',
                                            'crop'=> false
                                            )
                            );
                  
                  
                  
                  if ($action)  $this->set('image','/img/promotions/'.$fileName);
                  unset($this->data['Promotion']['fichero']);
              }
              
              
              
              //UPLOAD THE FLASH FILE 
              if (!empty($this->data['Promotion']['fichero_flash'])){
                  $f_fileName = str_pad($this->data['Promotion']['id'],10,'0',STR_PAD_LEFT).'.swf'; // Creamos el nombre del archivo
                  $action_f = move_uploaded_file($this->data['Promotion']['fichero_flash']['tmp_name'],'/img/promotions/'.$f_fileName);
                  if ($action_f) $this->set('flash_file','/img/promotions'.DS.$f_fileName);
                  unset($this->data['Promotion']['fichero_flash']);
              }
              
              
              //SAVE THE OVERALL RESULT
              if ((isset($action) and $action == true) or (isset($action_f) and $action_f == true))    $this->save();
                
        }//end for if(isset($this->data['Promotion']['id']))
  } 
  
  
  
  function beforeDelete(){
    
    $fileName = $this->field('image');
    $flashFileName = $this->field('flash_file');
    
    if(!is_dir(WWW_ROOT.$fileName) && file_exists(WWW_ROOT.$fileName))
        $success &= unlink(WWW_ROOT.$fileName);
    if(!is_dir(WWW_ROOT.$flashFileName) && file_exists(WWW_ROOT.$fileName))
        $success &= unlink(WWW_ROOT.$fileName);
        
        
    return true;
  }
  
  
  
  
}
?>