<?php
class Newsletter extends AppModel {

	var $name = 'Newsletter';
	var $validate = array(
		'email' => array(
		                'email'=>array(
		                	'rule' => 'email',
		                	'message' => 'Dirección de email inválida'
		                	),
		                'isUnique'=>array(
		                	'rule' => 'isUnique',
		                	'message' => 'Dirección ya dada de alta'
		                	),
		                )
	);

}
?>