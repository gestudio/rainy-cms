<?php
	class User extends AppModel {
		
		var $name = 'User';
		
		var $validate = array(
		    'password' => array(
			  'notEmpty'=> array('rule'=>'notempty','message'=>'Campo obligatorio','on'=>'create','required'=>true),
			),
			'passwordR' => array(
			  'passwordMatch' => array('rule'=>array('passwordMatch','password'),'message'=>'Debe coincidir con la contraseña'),
			  'notEmpty'=> array('rule'=>'notempty','message'=>'Campo obligatorio','on'=>'create'),
			  'minLength'=> array('rule'=>array('minLength',4),'message'=>'Al menos 4 carácteres','on'=>'create')
			),
			'email' => array(
			  'notEmpty'=> array('rule'=>'notempty','message'=>'Campo obligatorio'),
			  'isUnique'=>array('rule'=>'isUnique','message'=>'Dirección ya registrada'),
			  'isEmail'=>array('rule'=>'email','message'=>'Dirección de email inválida')
			),
			'conditions'=> array(
			  'isTrue'=> array('rule'=>'isTrue','on'=>'create','required'=>true,'message'=>'Debes aceptar el contrato de compra'),
			)
		);
		
		
		// ASSOCIATIONS
		var $belongsTo = array(
			'UserGroup' => array('className' => 'UserGroup','foreignKey' => 'user_group_id',)
		);
		
		var $hasMany = array();
		var $hasOne = array('UserDetail');
		
		
		var $hasAndBelongsToMany = array(
			'Document' => array(
				'className' => 'Document',
				'joinTable' => 'documents_users',
				'foreignKey' => 'user_id',
				'associationForeignKey' => 'document_id',
				'unique' => true,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'finderQuery' => '',
				'deleteQuery' => '',
				'insertQuery' => ''
			)
		);
	
	
	
	
	
	
	


  
		/***************** CALLBACKS *******************************************/
			
		function beforeValidate(){
			parent::beforeValidate();
			
			// Just sanitizes and formats the input 
			// --------------------------------------------------
			// Check all received params for correction such as:
			// $string = ucwords(mb_strtolower(trim($string),'utf-8');
			  
			if (!empty($this->data['User']['email'])) {
				$this->data['User']['email'] = (mb_strtolower(trim($this->data['User']['email']),'UTF-8'));
			} 
			
		}
		
		
		/**************** CUSTOM VALIDATION RULES *******************************/
		  
		  
		  
		  
		  
		  
		/**
		 * 
		 * Function passwordMatch()
		 * 
		 * @param object $check
		 * @param object $field
		 * 
		 * @description
		 * Hashes the passwordR field to compare it with password field.
		 * It requires full $this->data array with at least: password, passwordR and email keys.
		 * 
		 * @return 
		 * If both passwords are the same (bool)
		 * 
		 */
		function passwordMatch($check,$field){
			$hashedData = $this->hashPasswords(array('User'=>array('password'=>$this->data['User']['passwordR'],'email'=>$this->data['User']['email'])));
			return $hashedData['User']['password'] == $this->data['User'][$field];
		}
		
		
		
		
		
		
		
		
		
		/**
		 * 
		 * Function isTrue()
		 * 
		 * Just checks if fields value is true.
		 * 
		 */
		function isTrue($check){
			return current($check)==true;
		}
		
		
		
		
		
		
		
		
		
		
		/**************** CUSTOM MODEL FUNCTIONS *******************************/
		
		
		/**
		 * 
		 * Function hashPasswords()
		 * 
		 * @params
		 * $this->data array()
		 * 
		 * @description
		 * Hash the posted password to compare in database or save actions.
		 * The received password in $data array is modified and placed in the same array key.
		 * 
		 * @return
		 * Modified $this->data array with hashed password value.
		 */
		function hashPasswords($data) {
			if (isset($data['User']['password'])) {
				$data['User']['password'] = md5($data['User']['email'].$data['User']['password'].Configure::read('Security.salt'));
				return $data;
			}
			return $data;
		}
		
		
		
		
		
		/**
		 * 
		 * Function __randimize_password()
		 * 
		 * @params
		 * null
		 * 
		 * @description
		 * Generates a random password with 8 chars length
		 * 
		 * @return
		 * Returns new random password (string)
		 */
		function __randomize_password() {
		    $chars = "abcdefghijkmnopqrstuvwxyz0123456789";
		    srand((double)microtime()*1000000);
		    $i = 0;
		    $pass = '' ;
		
		    while ($i <= 7) {
		        $num = rand() % 33;
		        $tmp = substr($chars, $num, 1);
		        $pass = $pass . $tmp;
		        $i++;
		    }
		
		    return $pass;
		}
		
	}
?>