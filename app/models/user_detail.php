<?php
class UserDetail extends AppModel {
	var $name = 'UserDetail';
	var $displayField = 'name';
	var $virtualFields = array(
	    'full_name' => 'CONCAT(UserDetail.name, " ", UserDetail.surname)'
	);

	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dni' => array(
			'userdefined' => array(
				'rule' => array('valid_cif_nif'),
				'message' => 'DNI o NIF Incorrecto',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
		
	


  
		/***************** CALLBACKS *******************************************/
			
		function beforeValidate(){
			parent::beforeValidate();
			
			// Just sanitizes and formats the input 
			// --------------------------------------------------
			// Check all received params for correction such as:
			// $string = ucwords(mb_strtolower(trim($string),'utf-8');
			if (!empty($this->data['UserDetail'])) {
			
			    if (!empty($this->data['UserDetail']['name']))
				$this->data['UserDetail']['name'] = mb_convert_case(trim($this->data['UserDetail']['name']), MB_CASE_TITLE, 'UTF-8');    
			    if (!empty($this->data['UserDetail']['surname']))
				    $this->data['UserDetail']['surname'] = mb_convert_case(trim($this->data['UserDetail']['surname']), MB_CASE_TITLE, 'UTF-8');    
			    if (!empty($this->data['UserDetail']['company']))
				    $this->data['UserDetail']['company'] = mb_convert_case(trim($this->data['UserDetail']['company']), MB_CASE_TITLE, 'UTF-8');    
			    if (!empty($this->data['UserDetail']['address']))
				    $this->data['UserDetail']['address'] = mb_convert_case(trim($this->data['UserDetail']['address']), MB_CASE_TITLE, 'UTF-8');
			    if (!empty($this->data['UserDetail']['city']))
				    $this->data['UserDetail']['city'] = mb_convert_case(trim($this->data['UserDetail']['city']), MB_CASE_TITLE, 'UTF-8');
			    if (!empty($this->data['UserDetail']['pc']))
				    $this->data['UserDetail']['pc'] = ereg_replace('[^0-9]', '', $this->data['UserDetail']['pc']);
			    if (!empty($this->data['UserDetail']['phone']))
				    $this->data['UserDetail']['phone'] = ereg_replace('[^0-9]', '', $this->data['UserDetail']['phone']);
			    if (!empty($this->data['UserDetail']['mobile']))
				    $this->data['UserDetail']['mobile'] = ereg_replace('[^0-9]', '', $this->data['UserDetail']['mobile']);

			}  
			
			
		}
		
		
		/**************** CUSTOM VALIDATION RULES *******************************/
		  
		
		
		
		
		
		
		
		
		
		
		
	
	
	/*
	 * Function valid_nif
	 * 
	 * @description Checks it the provided NIF is valid or not
	 * @param $nif (String) user provided NIF
	 * @return valid_result (bool)
	 *
	 */
	function valid_nif($nif) {

		if (is_array($nif)) {
			$nif = array_values($nif);
			$nif = $nif[0];
		}
		$nif = strtoupper($nif);
		if (strlen($nif) != 9)
			return false;
		$letra = substr($nif, 8, 1);
		if (($letra < 'A') or ($letra > 'Z'))
			return false;
		$lnie = substr($nif, 0, 1);
		$numero = ($lnie == 'X' || $lnie == 'Y' || $lnie == 'Z') ? substr($nif, 1, 7) : substr($nif, 0, 8);
		if (!is_numeric($numero))
			return false;
		$l = substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(array('X', 'Y', 'Z'), array('0', '1', '2'), $nif), 0, 8) % 23, 1);
		return ($l == $letra);
	}

	
	
	
	
	
	
	
	
	
	/*
	 * Function valid_cif
	 * 
	 * @description Checks it the provided CIF is valid or not
	 * @param $cif (String) user provided CIF
	 * @return valid_result (bool)
	 *
	 */
	function valid_cif($cif) {
		if (is_array($cif)) {
			$cif = array_values($cif);
			$cif = $cif[0];
		}
		//    preg_match(".[0-9]*7/-[A-Za-z]");
		$cif = strtoupper($cif);
		if (strlen($cif) != 9)
			return false;
		$letra = substr($cif, 0, 1);
		if (($letra < 'A') or ($letra > 'Z'))
			return false;
		$lcif = substr($cif, 8, 1);
		$numero = substr($cif, 1, 7);
		if (!is_numeric($numero))
			return false;

		$cod_a = 0;
		$cod_b = 0;

		for ($contador = 0; $contador < strlen($numero); $contador++) {
			$digito = $numero{$contador};
			if ((($contador + 1) % 2) == 0) {
				$cod_a += $digito;
			} else {
				$cod_b += ( ($digito * 2) >= 10) ? (($digito * 2) - 10 + 1) : ($digito * 2);
			}
		}

		$cod_c = ($cod_a + $cod_b) % 10;

		$res = (10 - $cod_c) % 10;

		if (is_numeric($lcif)) {
			return ($res == $lcif);
		} else {
			$res2 = chr(ord('A') + $res);
			return ($res2 == $lcif);
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Function valid_cif_nif
	 * 
	 * @description Check if the provided String is a valid CIF or NIF
	 * @param $cif (String) provided value.
	 * @return valid (bool)
	 *
	 */
	function valid_cif_nif($cif) {
		return ($this->valid_cif($cif) || $this->valid_nif($cif));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
