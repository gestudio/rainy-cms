<?php
class Photo extends AppModel {
	var $name = 'Photo';
	var $displayField = 'file';
	
	
	
	var $validate = array(
		'file' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
