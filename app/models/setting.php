<?php


class Setting extends AppModel {
	
  function beforeSave(){
      $this->data['Setting']['group'] = strtoupper($this->data['Setting']['group']);
      $this->data['Setting']['key'] = strtoupper($this->data['Setting']['key']);
      return true;
  }
	
}
?>