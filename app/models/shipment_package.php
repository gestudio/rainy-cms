<?

class ShipmentPackage extends AppModel{
	var $name = 'ShipmentPackage';
	
	var $belongsTo = array('Shipment');
	
	
	var $validate = array(
		'shipment_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Error interno en número de envío.',
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				'required' => true,
				'last' => true, // Stop validation after this rule
			),
		),
		'quantity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe ser un número',
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
			)
		),
		'weight' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe ser un número',
				'allowEmpty'=>true,
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo obligatorio',
				'required' => true,
				'last' => true, // Stop validation after this rule
			)
		),
		'price' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe ser un número',
				'allowEmpty'=>true,
			),
		)
	);
	
	
	
}