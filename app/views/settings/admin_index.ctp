<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Configuración',1));
?>
<h1><?php __('Configuración');?></h1>


<div class="divided"><?php echo $paginator->counter(array('format' => __('%count% Registros. Mostrando del %start% al %end%.', true)));?></div>
<div class="divided pagination">
  <?php echo $paginator->prev('<< ');?>
  <?php echo $paginator->numbers();?>
  <?php echo $paginator->next(' >>');?>
</div>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('Grupo','group');?></th>
	<th><?php echo $paginator->sort('Clave','key');?></th>
	<th><?php echo $paginator->sort('Valor','value');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php

$i = 0;
foreach ($settings as $setting):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo ($setting['Setting']['group']); ?>
		</td>
		<td>
			<?php echo ($setting['Setting']['key']); ?>
		</td>
		<td>
			<?php echo $this->Text->truncate($setting['Setting']['value'],40); ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Editar', true), array('action' => 'edit', $setting['Setting']['id'])); ?>
			<?php echo $html->link(__('Eliminar', true), array('action' => 'delete', $setting['Setting']['id']), null, sprintf(__('Seguro desea eliminar irreversiblemente la clave %s ?', true), strtoupper($setting['Setting']['key']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
    

<div class="divided"><?php echo $paginator->counter(array('format' => __('%count% Registros. Mostrando del %start% al %end%.', true)));?></div>
<div class="divided pagination">
	<?php echo $paginator->prev('<< ');?>
	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(' >>');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Crear nueva variable de configuración', true), array('action' => 'add'),array('class'=>'button add')); ?></li>
	</ul>
</div>
