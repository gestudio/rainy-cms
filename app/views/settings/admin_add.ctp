<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Configuracion',1),'/admin/settings');
$this->Html->addCrumb(__('Nueva variable de configuración',1));
?>
<h1><? __('Nueva variable de configuración'); ?></h1>

<div class="settings form">
<?php echo $form->create('Setting');?>
	<fieldset>
 		<legend><?php __('Datos de la variable');?></legend>
	<?php
    echo $form->input('group',array('label'=>__('Grupo',1)));
    echo $form->input('key',array('label'=>__('Clave',1)));
    echo $form->input('description',array('label'=>__('Descripción',1)));
    echo $form->input('value',array('label'=>__('Valor',1)));
	?>
	</fieldset>
<?php echo $form->end(__('Guardar',1));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Volver a configuración', true), array('action' => 'index'));?></li>
	</ul>
</div>
