<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Configuracion',1),'/admin/settings');
$this->Html->addCrumb(__('Editar variable de configuración',1));
?>
<h1><?php __('Editar variable de configuración');?></h1>
<?php echo $form->create('Setting');?>
	<fieldset>
 		<legend><?php __('Editar variable de configuración');?></legend>
	<?php
	
		echo $form->input('id');
		echo $form->input('group',array('label'=>__('Grupo',1)));
		echo $form->input('key',array('label'=>__('Clave',1)));
		echo $form->input('description',array('label'=>__('Descripción',1)));
		echo $form->input('value',array('label'=>__('Valor',1)));
	?>
	</fieldset>
<?php echo $form->end(__('Guardar',1));?>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Eliminar', true), array('action' => 'delete', $form->value('Setting.id')), array('class'=>'button'), sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Setting.id'))); ?></li>
		<li><?php echo $html->link(__('Volver a configuración', true), array('action' => 'index'), array('class'=>'button'));?></li>
	</ul>
</div>
