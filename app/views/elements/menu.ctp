<?

$categoriesForMenu = $this->requestAction('/NavigationMenus/getMenu', array('cache' => '+1 hour'));

echo '<ul class="navMenu">';
foreach ($categoriesForMenu as $category):
	
	echo '<li>';
	echo $html->link(__d('menu',$category['NavigationMenu']['name'],1), $category['NavigationMenu']['url']);
	
	if(!empty($category['children']) and !isset($noDropDowns)){
		echo '<ul class="subnav">';
		foreach ($category['children'] as $child){
			echo '<li>';
			echo $html->link(__d('menu',$child['NavigationMenu']['name'],1), $child['NavigationMenu']['url']);
			
			
			if(!empty($child['children'])){
				echo '<ul>';
				foreach ($child['children'] as $childy){
					echo $this->Html->tag('li',$html->link(__d('menu',$childy['NavigationMenu']['name'],1), $childy['NavigationMenu']['url'], array('class' => '')));


				}
				echo '</ul>';
			}
			echo '</li>';
			
		}
		echo '</ul>';
	}
	
	echo '</li>';
endforeach;
echo '</ul>';

if(!isset($noDropDowns)) echo $this->Html->script('menu');