<p>Te han enviado un nuevo documento en <?=Configure::read('SITE.TITLE');?>.</p>


<p>
<strong>Nombre:</strong> <?=$document['Document']['name'];?><br />
<strong>Descripción:</strong>
<br /><br />
<?=nl2br($document['Document']['description']);?>
</p>

<p>El documento esta adjunto en este correo electrónico, pero también puedes descargartelo desde tu área de cliente:<br />
    <a href="<?=Configure::read('Security.protocol')?>://<?=$_SERVER['SERVER_NAME'];?><?=$this->Html->url('/users/login');?>" title="Accede a tu área de usuario">http://<?=$_SERVER['SERVER_NAME'];?>/<?=$this->Html->url('/users/login');?></a>
</p>