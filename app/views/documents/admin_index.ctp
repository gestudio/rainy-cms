<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Documentos',1));
?><div class="documents index">
	<h2><?php __('Documentos');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort(__('Remitente',1),'user_id');?></th>
			<th><?php echo $this->Paginator->sort(__('Destinatario(s)',1),'user_id');?></th>
			<th><?php echo $this->Paginator->sort(__('Documento',1),'name');?></th>
			<th><?php echo $this->Paginator->sort(__('Fecha',1),'created');?></th>
			<th class="actions"></th>
	</tr>
	<?php
	$i = 0;
	foreach ($documents as $document):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td>
		    <?=$document['User']['UserDetail']['full_name']?>
		<?php echo $this->Html->link($document['User']['email'], array('controller' => 'users', 'action' => 'view', $document['User']['id'])); ?>
		</td>
		<td><?php 
		foreach ($document['Recipient'] as $recipient){
		    echo $recipient['email'].'<br />';
		}
		 ?></td>
		<td><?php 
		$icon = 'icons'.DS.$document['Document']['ext'].'.png';
		
		if (file_exists(IMAGES.$icon))
		    echo $this->Html->image($icon);
		else
		    echo $this->Html->image('icons/txt.png');
		echo $this->Html->link($document['Document']['name'],'/files/'.base64_encode($document['Document']['id']).'.'.$document['Document']['ext']); ?>&nbsp;</td>
		<td><?php echo date('H:i d-m-Y',$document['Document']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $document['Document']['id'])); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('action' => 'delete', $document['Document']['id']), null, sprintf(__('Seguro deseas eliminar el documento "%s" ?', true), $document['Document']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('%current% docuemntos de %count%. Mostrando %start% al %end%.', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anteriores', true), array(), null, array('class'=>'disabled'));?>
	 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('siguientes', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo documento', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Crear nuevo usuario', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>