<h1><?__('Acceso a documentación privada');?></h1>
<? $this->Html->addCrumb(__('Documentación',1));?>

<div class="grid_6 alpha"><?=$this->Html->image('layout/documents.jpg',array('width'=>'390'));?></div>
<div class="grid_10 omega">
<p><?__('Para poder ver su documentación introduzca su dirección de correo electrónico y contraseña en el formulario por favor.');?></p>

<?
echo $this->Form->create('User',array('url'=>'/documents'));
echo $this->Form->input('email',array('label'=>__('Email',1)));
echo $this->Form->input('password',array('label'=>__('Clave de acceso',1),'after'=>'<a href="'.$this->Html->url('/users/password_recover').'" class="thickbox" title="'.__('Recuperar su contraseña',1).'">'.__('He olvidado mi contraseña',1).'</a>'));
echo $this->Form->end(__('Enviar',1));
?>
</div>