<h1><?__('Añadir documento');?></h1>
<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Documentos',1), '/admin/documents');
$this->Html->addCrumb(__('Añadir documento',1));
?>
<div class="documents form">
<?php echo $this->Form->create('Document',array('type'=>'file'));?>
	<fieldset>
		<legend><?php __('Detalles del documento'); ?></legend>
	<?php
		echo $this->Form->input('user_id',array('type'=>'hidden','value'=>$this->Session->read('Auth.User.id')));
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('file',array('label'=>__('Archivo',1),'type'=>'file'));
		echo $this->Form->input('Recipient',array('label'=>__('Destinatarios',1),'multiple'=>true,'options'=>$users));
		echo $this->Form->input('notify',array('label'=>__('Notificar por email a los destinatarios',1),'type'=>'checkbox'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar', true));?>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Ver Documentos', true), array('action' => 'index'));?></li>
	</ul>
</div>