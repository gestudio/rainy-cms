<h1><?__('Añadir documento');?></h1>
<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Documentos',1), '/admin/documents');
$this->Html->addCrumb(String::insert(__('Editar documento de :usuario',1),array('usuario'=>$this->data['User']['email'])));
?>
<div class="documents form">
<?php echo $this->Form->create('Document',array('type'=>'file'));?>
	<fieldset>
		<legend><?php __('Detalles del documento'); ?></legend>
	<?php
		echo $this->Form->input('Document.id');
		echo $this->Form->input('user_id',array('type'=>'hidden','value'=>$this->Session->read('Auth.User.id')));
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>	
		<p><strong><?__('Archivo adjunto');?>:</strong><br />
		<?
		
		$icon = 'icons'.DS.$this->data['Document']['ext'].'.png';
		$doc = '/files/'.base64_encode($this->data['Document']['id']).'.'.$this->data['Document']['ext'];
		if (file_exists(IMAGES.$icon))
		    echo $this->Html->image($icon);
		else
		    echo $this->Html->image('icons/txt.png');
		echo $this->Html->link($this->data['Document']['name'],$doc);?> <?
		echo $this->Custom->FileSize(filesize(WWW_ROOT.$doc));
		?>
		    <br /><cite><?__('Los archivos adjuntos no se pueden editar dado que ya han sido enviados por email.');?><br />
			<?__('Si desea puede');?> <?=$this->Html->link(__('eliminar el documento',1),array('action'=>'delete',$this->data['Document']['id']),array('confirm'=>String::insert(__('¿Seguro desea eliminar el documento ":documento" ?',1),array('documento'=>$this->data['Document']['name']))));?>.</cite>
			
		</p>
	<?	
		echo $this->Form->input('Recipient',array('label'=>__('Destinatarios',1),'multiple'=>true,'options'=>$users));
		//echo $this->Form->input('notify',array('label'=>__('Notificar por email a los destinatarios',1),'type'=>'checkbox'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar', true));?>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Ver Documentos', true), array('action' => 'index'));?></li>
	</ul>
</div>