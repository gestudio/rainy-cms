<h1><?=$this->pageTitle=__('Documentación guardada',true)?></h1>
<? $this->Html->addCrumb(__('Documentación',1), '/documents');
$this->Html->addCrumb(__('Mis documentos',1));
?>
<div class="grid_15 pull_2 push_1" style="background: #ececec; padding:1%;margin-bottom: 20px;">
    <div class="grid_7 alpha">
	<?=$user['UserDetail']['full_name'];?> (<?=$user['UserDetail']['dni'];?>)<br />
	<?=$user['UserDetail']['company'];?><br />
	<?=$user['UserDetail']['phone'];?> <?=$user['UserDetail']['mobile'];?><br />
 	<strong><? echo __('Correo electrónico',true); ?></strong> <?=$user['User']['email'];?>
    </div>
    <div class="grid_7 omega">
	<?=nl2br($user['UserDetail']['address']);?><br />
	<?=$user['UserDetail']['pc'];?> <?=$user['UserDetail']['city'];?><br />
	 <?=$user['UserDetail']['province'];?><br />
	<strong><? echo __('Grupo de usuarios',true);?></strong>  <?=$user['UserGroup']['name'];?>
    </div>
</div>

<h2><?__('Documentos');?> ( <?=count($user['Document']);?> )</h2>
<div class="grid_15" style="background: #ececec; padding:1%;">
    
    
    <table cellpadding="0" cellspacing="0">
	<tr>
			<? /*<th><?php echo __('Remitente',1);?></th>*/?>
			<th><?php echo __('Documento',1);?></th>
			<th><?php echo __('Fecha',1);?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($user['Document'] as $document):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<? /*<td>
		    <?=$document['User']['UserDetail']['full_name']?>
		<?php echo $this->Html->link($document['User']['email'], array('controller' => 'users', 'action' => 'view', $document['User']['id'])); ?>
		</td>*/
		?>
		<td><?php 
		$icon = 'icons'.DS.$document['ext'].'.png';
		
		if (file_exists(IMAGES.$icon))
		    echo $this->Html->image($icon);
		else
		    echo $this->Html->image('icons/txt.png');
		echo $this->Html->link($document['name'],'/files/'.base64_encode($document['id']).'.'.$document['ext']); ?><br />
		<?=nl2br($document['description'])?></td>
		<td><?php echo date('H:i d-m-Y',$document['created']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
    
    
</div>
