<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Fotos',1));
?>
<div class="photos index">
	<h1><?php __('Fotos');?></h1>
	
	<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nueva foto', true), array('action' => 'add')); ?></li>
	</ul>
</div>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort(__('Foto',1),'id');?></th>
			<th><?php echo $this->Paginator->sort(__('Descripción',1),'description');?></th>
			<th><?php echo $this->Paginator->sort(__('Alta',1),'created');?></th>
			<th class="actions"></th>
	</tr>
	<?php
	$i = 0;
	foreach ($photos as $photo):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $this->Html->image('uploads/resized/'.$photo['Photo']['file']); ?></td>
		<td><?php echo $this->Text->truncate($photo['Photo']['description'],400); ?>&nbsp;</td>
		<td><?php echo date('d-M',$photo['Photo']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $photo['Photo']['id'])); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('action' => 'delete', $photo['Photo']['id']), null, sprintf(__('¿Seguro deseas eliminar esta foto?', true), $photo['Photo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Mostrando %current% fotos de %count% total.', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anteriores', true), array(), null, array('class'=>'disabled'));?>
	  	<?php echo $this->Paginator->numbers();?>
 
		<?php echo $this->Paginator->next(__('siguientes', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nueva foto', true), array('action' => 'add')); ?></li>
	</ul>
</div>