<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Fotos',1),'/admin/photos');
$this->Html->addCrumb(__('Editar foto',1));
?>
<h1><?=__('Editar foto',1)?></h1>

<div class="photos form">
<?php echo $this->Form->create('Photo');?>
	<fieldset>
		<legend><?php __('Admin Edit Photo'); ?></legend>
		<div class="grid_3 alpha"><?php echo $this->Html->image('uploads/resized/'.$this->data['Photo']['file']); ?></div>
		<div class="grid_12 omega">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description',array('label'=>__('Descripción',1)));
	?></div>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Eliminar esta foto', true), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('¿Seguro deseas eliminar esta foto?', true)); ?></li>
		<li><?php echo $this->Html->link(__('Ver fotos', true), array('action' => 'index'));?></li>
	</ul>
</div>