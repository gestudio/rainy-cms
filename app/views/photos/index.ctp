<? $this->Html->addCrumb(__('Fotos',1)); ?>
<h1><?=$title_for_layout?></h2>
<div class="photos index">
	<div class="photoGallery">
	<?php foreach ($photos as $photo): ?>
	    <a href="<?=$this->Html->url('/photos/view/'.$photo['Photo']['id']);?>" class="" title="<?php echo $this->Text->truncate($photo['Photo']['description'],150); ?>">
		<?php echo $this->Html->image('uploads/resized/'.$photo['Photo']['file']); ?>
	    </a>
	<?php endforeach; ?>
	</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Mostrando %current% fotos de %count% en total.', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anteriores', true), array(), null, array('class'=>'disabled'));?>
	 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('siguientes', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>