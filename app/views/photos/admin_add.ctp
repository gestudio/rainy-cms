<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Fotos',1),'/admin/photos');
$this->Html->addCrumb(__('Nueva foto',1));
?>
<h1><?=__('Nueva foto',1)?></h1>
<div class="photos form">
    <div id="imgSpinner" class="grid_16" style="display:none;"><h2><?=$this->Html->image('loadingAnimation.gif');?><?__('Cargando');?>...</h2></div>
<?php echo $this->Form->create('Photo',array('type'=>'file','id'=>'imgUpload','class'=>'grid_16'));?>
	<fieldset>
		<legend><?php __('Nueva foto'); ?></legend>
		<div class="grid_4 alpha">
		    <?=$this->Html->image('admin/camera.jpg',array('id'=>'fotoPreview'));?>
		</div>
		<div class="grid_8 omega">
		    <?php
			    echo $this->Form->input('file',array('label'=>__('Foto',1),'type'=>'file','id'=>'doUpload'));
			    echo $this->Form->input('description',array('label'=>__('Descripción de la foto',1)));

		    ?>
		    
		</div>
		<?php echo $this->Form->end(__('Guardar', true));?>
		
	</fieldset>

    
</div>

<script type="text/javascript">
    $('#imgUpload').submit(function(){
	$('div.submit input').attr("disabled","disabled")
	$(this).hide("slow");
	$('#imgSpinner').show('slow');
});


function validar(f)	{
	enviar = /.(gif|jpg|png)$/i.test(f.archivo.value);
	if (!enviar)	alert("seleccione imagen");
	return enviar;
}

function DoPreview(){
    var filename = document.Form1.fdSubirImg.value;
    var Img = new Image();
    if (navigator.appName == "Netscape"){
	alert("Previews do not work in Netscape.");
    }else{
	Img.src = filename;
	$('#fotoPreview').src = Img.src;
    }
}

</script>