<div class="grid_2"><?
echo $html->link(__('Add Menu item',1),array('action'=>'add'),array('class'=>'btn'));
?>
	
</div>

<div class="grid_10">
<?
echo "<ul>";
foreach($menu as $key=>$value){
    $editurl = $html->link($this->Html->image('icons/edit-hyphenation.png',array('alt'=>'Edit')), array('action'=>'edit', $key),array('escape'=>false));
    $upurl = $html->link($this->Html->image('icons/arrow-090-small.png',array('alt'=>'Up')), array('action'=>'moveup', $key),array('escape'=>false));
    $downurl = $html->link($this->Html->image('icons/arrow-270-small.png',array('alt'=>'Down')), array('action'=>'movedown', $key),array('escape'=>false));
    $deleteurl = $html->link($this->Html->image('icons/cross-small.png',array('alt'=>'delete')), array('action'=>'delete', $key),array('escape'=>false));
    echo "<li>$editurl $upurl $downurl $deleteurl $value</li>";
}
echo "</ul>";
?>
</div>