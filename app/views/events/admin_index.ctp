<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Eventos',1));
?>
<div class="events index">
	<h2><?php __('Eventos');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort(__('Nombre',1),'name');?></th>
			<th><?php echo $this->Paginator->sort(__('Descripción',1),'description');?></th>
			<th><?php echo $this->Paginator->sort(__('Fecha',1),'date');?></th>
			<th><?php echo $this->Paginator->sort(__('Alta',1),'created');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($events as $event):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $event['Event']['name']; ?>&nbsp;</td>
		<td><?php echo $this->Text->truncate($event['Event']['description'],40); ?>&nbsp;</td>
		<td><?php echo $event['Event']['date']; ?>&nbsp;</td>
		<td><?php echo date('Y-m-d',$event['Event']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $event['Event']['id'])); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('action' => 'delete', $event['Event']['id']), null, sprintf(__('Seguro deseas eliminar el evento - %s -?', true), $event['Event']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Mostrando %current% eventos de %count%. Eventos %start% al %end%.', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anteriores', true), array(), null, array('class'=>'disabled'));?>
	 	<?php echo $this->Paginator->numbers();?>
 
		<?php echo $this->Paginator->next(__('siguientes', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo evento', true), array('action' => 'add')); ?></li>
	</ul>
</div>