<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Eventos',1),'/admin/events');
$this->Html->addCrumb(__('Crear nuevo evento',1));
?><h1><?__('Crear nuevo evento');?></h1>
<div class="events form">
<?php echo $this->Form->create('Event');?>
	<fieldset>
		<legend><?php __('Datos del evento'); ?></legend>
	<?php
		echo $this->Form->input('name',array('label'=>__('Nombre',1)));
		echo $this->Form->input('description',array('label'=>__('Descripción',1)));
		echo $this->Form->input('date',array('label'=>__('Fecha',1)));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar', true));?>
</div>
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Ver eventos', true), array('action' => 'index'));?></li>
	</ul>
</div>