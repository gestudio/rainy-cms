<? $this->Html->addCrumb(__('Eventos',1)); ?>

	<h1><?php __('Eventos');?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort(__('Nombre',1),'name');?></th>
			<th><?php echo $this->Paginator->sort(__('Descripción',1),'description');?></th>
			<th><?php echo $this->Paginator->sort(__('Fecha',1),'date');?></th>

	</tr>
	<?php
	$i = 0;
	foreach ($events as $event):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $event['Event']['name']; ?>&nbsp;</td>
		<td><?php echo $event['Event']['description']; ?>&nbsp;</td>
		<td><?php echo $event['Event']['date']; ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Eventos %start% al %end%. Total %count%.', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anteriores', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('siguientes', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>