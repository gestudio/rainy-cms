<? $this->Html->addCrumb(__('Noticias',1),'/news'); ?>
<? $this->Html->addCrumb(__('Twitter',1)); ?>
<h1 class="heading"><?__('Canal de noticias');?></h1>
<p class="important">Siguenos en nuestro canal de noticias Twitter: <cite><?=$html->link('http://www.twitter.com/'.Configure::read('SOCIAL.TWITTER'),'http://www.twitter.com/'.Configure::read('SOCIAL.TWITTER'));?></cite></p>
<?
if (!empty($items)){
  foreach($items as $item) {
    ?>
    <hr/>
    <p>
      <small><?=date('D / m / Y',strtotime($item->get_date()));?></small><br/>
      <?=$html->link($item->get_title(), $item->get_permalink(),array('class'=>'twitter-link'))?>
    </p>
    <?
  }
}else{
  echo '<h2> Aún no hay noticias.</h2>';
}
?>