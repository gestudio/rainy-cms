<div class="grid_16">
	<p><? __('Últimas noticias'); ?></p>
<hr />

    <?php foreach ($news as $i => $newsItem) : ?>
	<div class="grid_6 alpha">		

	    <h3><?php echo $newsItem['RssNews']['title']; ?></h3>
	    <p class="subtitle"><?php echo date('Y.m.d', strtotime($newsItem['RssNews']['pubDate'])); ?></p>
	    <?php echo $newsItem['RssNews']['description']; ?>

	</div>		
    <?php endforeach;?>

</div>