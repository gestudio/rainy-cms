<?php

/**
 * Custom Helper
 *
 * For custom theme specific methods.
 *
 * If your theme requires custom methods,
 * copy this file to /app/views/themed/your_theme_alias/helpers/custom.php and modify.
 *
 * You can then use this helper from your theme's views using $custom variable.
 *
 * @category Helper
 * @package  Gestudio CMS
 * @version  1.0
 */
class CustomHelper extends Helper {

    /**
     * Other helpers used by this helper
     *
     * @var array
     * @access public
     */
    public $helpers = array();

    /**
     * Status
     *
     * instead of 0/1, show tick/cross
     *
     * @param integer $value 0 or 1
     * @return string formatted img tag
     */
    public function status($value) {
	if ($value == 1) {
	    $output = $this->Html->image('/img/icons/tick.png');
	} else {
	    $output = $this->Html->image('/img/icons/cross.png');
	}
	return $output;
    }

    public function FileSize($FZ, $setup = null) {
	$FS = array("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
	$i=null;
	if (!$setup && $setup !== 0) {
	    return number_format($FZ / pow(1024, $I = floor(log($FZ, 1024))), ($i >= 1) ? 2 : 0) . ' ' . $FS[$I];
	} elseif ($setup == 'INT')
	    return number_format($FZ);
	else
	    return number_format($FZ / pow(1024, $setup), ($setup >= 1) ? 2 : 0 ) . ' ' . $FS[$setup];
    }

}

?>