<?
$this->Html->addCrumb($title_for_layout);
?>

<div class="grid_3 alpha"><?=$html->image('layout/secure.png');?></div>
<div class="grid_12 omega">﻿<h1><?=$title_for_layout; ?></h1>
<p><? __('Tu sesión se ha cerrado correctamente, por lo que nadie podrá acceder a tus datos personales ni comerciales.'); ?><br />
  <? __('Para continuar con tus gestiones o realizar una nueva compra tendrás que volver a acceder a tu cuenta usando tus datos personales.'); ?>
</p>
</div>