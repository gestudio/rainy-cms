<h1><?__('Recuperar contraseña olvidada');?></h1>
<div id="ajaxUpdateable">
<p style="font-weight: bold"><? __('Si has olvidado tu contraseña de acceso estas en el sitio adecuado.');?><p/>
<p><?__('Debes facilitar la dirección de correo electrónico con la que te registraste en '.Configure::read('SITE.TITLE').'.'); ?><br />
<?__('Te crearemos una nueva clave y te la enviaremos a esa misma dirección de correo electrónico.');?></p>
<fieldset class="grid_6">
	<legend>Rellena el formulario con tus datos.</legend>  
	<?php echo $this->Form->create('User', array('url' => '/users/password_recover','id'=>'passwordRecover')); ?>
	<?php echo $this->Form->input('email', array('label' => 'Correo electrónico'));  ?>
	<?php echo $this->Js->submit(__('Enviar',1),array('update'=>'#ajaxUpdateable')); ?>
	<?php echo $this->Form->end();?>		
</fieldset> 

</div>