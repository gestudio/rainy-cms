<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Usuarios',1),'/admin/users');
$this->Html->addCrumb($title_for_layout);
?>


<h1><?=$title_for_layout?></h1>

<? echo $form->create();?>
<? echo $form->input('User.id'); ?>
<? echo $form->input('UserDetail.id'); ?>

<div class="grid_15 pull_2 push_1" style="background: #ececec; padding:1%;margin-bottom: 20px;">
    <div class="grid_7 alpha">
	<? echo $form->input('User.email',array('label'=>__('Correo electrónico',true))); ?>
    </div>
    <div class="grid_7 omega">
	<? echo $form->input('User.user_group_id',array('label'=>__('Grupo de usuarios',true)));?>
    </div>
    
    <div class="grid_15"><? echo $form->input('User.bulletin',array('label'=>__('¿desea recibir el boletín?',true)));?></div>
</div>


<div class="grid_15" style="background: #ececec; padding:1%;">
    <div class="grid_6 alpha">
	<?    
	echo $form->input('UserDetail.name',array('label'=>__('Nombre',true)));
	echo $form->input('UserDetail.surname',array('label'=>__('Apellidos',true)));
	echo $form->input('UserDetail.dni',array('label'=>__('DNI o NIF',true)));
	echo $form->input('UserDetail.company',array('label'=>__('Empresa',true)));
	echo $form->input('UserDetail.phone',array('label'=>__('Teléfono',true)));
	echo $form->input('UserDetail.mobile',array('label'=>__('Móvil',true)));

	?>
    </div>
    <div class="grid_9 omega">
    <?
    echo $form->input('UserDetail.address',array('label'=>__('Dirección',true)));
    echo $form->input('UserDetail.city',array('label'=>__('Ciudad',true)));
    echo $form->input('UserDetail.pc',array('label'=>__('Código Postal',true)));
    echo $form->input('UserDetail.province',array('label'=>__('Provincia',true)));
    echo $form->input('UserDetail.remarks',array('label'=>__('Notas adicionales',true)));
    ?>
    </div>
</div>

<hr />


    
<?
echo $form->end(__('Guardar datos',true));
?>
