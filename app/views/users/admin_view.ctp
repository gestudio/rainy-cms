<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Usuarios',1),'/admin/users');
$this->Html->addCrumb($title_for_layout);
?>
<h1><?=$title_for_layout?></h1>


<div class="grid_15 pull_2 push_1" style="background: #ececec; padding:1%;margin-bottom: 20px;">
    <div class="grid_7 alpha">
	<?=$user['UserDetail']['full_name'];?> (<?=$user['UserDetail']['dni'];?>)<br />
	<?=$user['UserDetail']['company'];?><br />
	<?=$user['UserDetail']['phone'];?> <?=$user['UserDetail']['mobile'];?><br />
 	<strong><? echo __('Correo electrónico',true); ?></strong> <?=$user['User']['email'];?>
    </div>
    <div class="grid_7 omega">
	<?=nl2br($user['UserDetail']['address']);?><br />
	<?=$user['UserDetail']['pc'];?> <?=$user['UserDetail']['city'];?><br />
	 <?=$user['UserDetail']['province'];?><br />
	<strong><? echo __('Grupo de usuarios',true);?></strong>  <?=$user['UserGroup']['name'];?>
    </div>
</div>

<h2><?__('Documentos');?> ( <?=count($user['Document']);?> )</h2>
<div class="grid_15" style="background: #ececec; padding:1%;">
    
    
    <table cellpadding="0" cellspacing="0">
	<tr>
			<? /*<th><?php echo __('Remitente',1);?></th>*/?>
			<th><?php echo __('Documento',1);?></th>
			<th><?php echo __('Fecha',1);?></th>
			<th class="actions"></th>
	</tr>
	<?php
	$i = 0;
	foreach ($user['Document'] as $document):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<? /*<td>
		    <?=$document['User']['UserDetail']['full_name']?>
		<?php echo $this->Html->link($document['User']['email'], array('controller' => 'users', 'action' => 'view', $document['User']['id'])); ?>
		</td>*/
		?>
		<td><?php 
		$icon = 'icons'.DS.$document['ext'].'.png';
		
		if (file_exists(IMAGES.$icon))
		    echo $this->Html->image($icon);
		else
		    echo $this->Html->image('icons/txt.png');
		echo $this->Html->link($document['name'],'/files/'.base64_encode($document['id']).'.'.$document['ext']); ?><br />
		<?=nl2br($document['description'])?></td>
		<td><?php echo date('H:i d-m-Y',$document['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver', true), array('action' => 'view', $document['id'])); ?>
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $document['id'])); ?>
			<?php echo $this->Html->link(__('Eliminar', true), array('action' => 'delete', $document['id']), null, sprintf(__('Seguro deseas eliminar el documento - # -  %s?', true), $document['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
    
    
</div>
<hr />
<div class="actions">
	<h3><?php __('Navegación'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Editar usuario', true), array('action' => 'edit',$user['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Cambiar la contraseña', true), array('action' => 'change_password',$user['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Enviarle un documento', true), array('controller' => 'documents', 'action' => 'add','user'=>$user['User']['id'])); ?> </li>
	</ul>
</div>
