<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Usuarios',1),'/admin/users');
$this->Html->addCrumb($title_for_layout);
?>

<h1><?=$title_for_layout?></h1>
<p><?=String::insert(__('Se dispone a cambiar la contraseña de la cuenta: :cuenta',1),array('cuenta'=>$this->data['User']['email']));?></p>
<?
e($form->create('User',array('url'=>array('action'=>'change_password','controller'=>'users'))));
e($form->input('id',array('value'=>$this->data['User']['id'])));
e($form->input('email',array('type'=>'hidden')));
e($form->input('password',array('label'=>__('Nueva contraseña.',true))));
e($form->input('passwordR',array('label'=>__('Verificar nueva contraseña.',true),'type'=>'password')));
e($form->end(__('Cambiar',true)));
?>