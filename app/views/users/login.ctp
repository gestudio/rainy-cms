<?
$this->Html->addCrumb($title_for_layout);
?>




<div class="grid_4 alpha"><br /><br /><br /><br />
<?=$this->Html->image('layout/secure.png');?>
</div>

<div class="grid_12 omega">
<h1><?=$title_for_layout?></h1>
<?
$session->flash('auth');
echo $form->create('User', array('url' => '/users/login'));
?>
<fieldset>
	<legend><? __('Introduce tus datos para acceder a tu área de usuario'); ?></legend>  
	
	<?php
	
	echo $form->input('email', array('label' => __('Correo electrónico',1))); //text
	echo $form->input('password', array('label' => __('Contraseña',1),'after'=>'<a href="'.$html->url('/users/password_recover').'" class="thickbox" title="'.__('Crea una nueva clave para acceder',true).'">'.__('He olvidado mi contraseña',true).'.</a>')); //password
	echo $form->input('remember_me', array('label' => __('Recordar contraseña',1),'type'=>'checkbox')); //Checkbox recordarme!
	echo $form->end('Enviar');
	?>
</fieldset>
</div>