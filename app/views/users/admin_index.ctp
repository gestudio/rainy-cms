<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb($title_for_layout);
?>
<h1><?=$title_for_layout?></h1>

<ul class="button-list">
	<li><?=$html->link('Crear nuevo usuario',array('action'=>'add'),array('class'=>'button add'))?></li>
</ul>
<div class="clear"></div>

<? if(!empty($users)): ?>
	
	<?
	
	// Opciones de la paginación
	$paginator->options(array(
		'url'=>array_merge($this->params['pass'],$this->params['named']),
		//'model'=>'Customer',
		'indicator' => 'LoadingDiv'
	));
	?>
	
	<table>
		<thead>
			<tr>
			    <th><?=$paginator->sort('Nombre y Apellidos', 'UserDetail.name')?></th>
				<th><?=$paginator->sort('Correo electrónico', 'email')?></th>
				<th><?=$paginator->sort('Grupo', 'UserGroup.name')?></th>
				<th><?=$paginator->sort('Recibe boletín', 'bulletin')?></th>
				<th><?=$paginator->sort('Fecha de creación', 'created')?></th>
				<th><?=$paginator->sort('Último acceso', 'last_login')?></th>
				<th><? __('opciones') ?></th>
			</tr>
		</thead>
		<tbody>
			<?
			foreach($users as $user){
				?>
				<tr>
				    <td><?=$this->Html->link($user['UserDetail']['name'].' '.$user['UserDetail']['surname'],array('action'=>'admin_view',$user['User']['id']),array('title'=>__('Ver detalles del usuario',1)))?></td>
					<td><?=$this->Html->link($user['User']['email'],'mailto:'.$user['User']['email'],array('title'=>__('Enviar correo electrónico al usuario',1)))?></td>
					<td><?=$user['UserGroup']['name']?></td>
					<td class="boolean"><?=$user['User']['bulletin']?'<span class="yes">Si</span>':'<span class="no">No</span>'?></td>
					<td><?=date('H:i d-m-Y',$user['User']['created'])?></td>
					<td><?=date('H:i d-m-Y',$user['User']['last_login'])?></td>
					<td>
						<?=$this->Html->link(
							__('Editar',true),
							array('action'=>'admin_edit',$user['User']['id']),
							array('class'=>'opt edit','title'=>__('Editar datos de usuario',true))
						)?>
						<?=$this->Html->link(
							__('Clave',true),
							array('action'=>'admin_change_password',$user['User']['id']),
							array('class'=>'opt del','title'=>__('Cambiar la contraseña',true))
						)?>
						<?=$this->Html->link(
							__('Eliminar',true),
							array('action'=>'delete',$user['User']['id']),
							array('class'=>'opt del','title'=>__('Eliminar',true)),
							str_replace('[[UserName]]',$user['User']['email'],__('¿Seguro que quieres borrar el usuario "[[UserName]]"?',true))
						)?>
					</td>
				</tr>
				<?
			}
			?>
		</tbody>
	</table>
	
	<?=$paginator->prev('<<')?>
	<?=$paginator->numbers()?>
	<?=$paginator->next('>>')?>

<? else: ?>
	<div class="empty-query">No hay resultados para esa búsqueda.</div>
<? endif; ?>