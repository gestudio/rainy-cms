<?
$this->Html->addCrumb(__('Boletin de noticias',1));
?>
<h1><?__('Boletin electrónico.'); ?></h1>

  <p><? __('Inscribase a nuestro boletín y periodicamente le enviaremos a su buzón de correo electrónico una serie de ofertas, y productos especiales únicamente disponibles para nuestros usuarios registrados.');?></p>
      <?
	
	if(isset($this->params['url']['emailsub']) and $this->params['url']['emailsub'] != 'Subscribirme a newsletter') $this->data['Newsletter']['name'] = $this->params['url']['emailsub'];
	elseif((!isset($this->params['url']['emailsub']) and empty($this->data)) or  $this->params['url']['emailsub'] == 'Subscribirme a newsletter') $this->data['Newsletter']['name']  = 'Tu nombre';
	
	if(empty($this->data['Newsletter']['email']))$this->data['Newsletter']['email'] = 'Tu email';
	
        echo $form->create('Newsletter',array('action'=>'newsletters','action'=>'index')); 
        ?>
        <fieldset>
        <legend><?__('Inscripción en el boletín de ofertas');?></legend>
        <?    	
        echo $form->input('name',array('id'=>'newsletterNameField2','value'=>$this->data['Newsletter']['name'],'label'=>__('Nombre:',true)));
        echo $form->input('email',array('id'=>'newsletterEmailField2','value'=>$this->data['Newsletter']['email'],'label'=>__('Correo electríonico:',true)));
        echo $form->input('conditions', array('label'=>__('Acepto la '.$html->link(__('nota legal',1),'/cuba/legal').' de subscripción.',true),'type'=>'checkbox'));
        ?>
        </fieldset>  
        <? echo $form->end(__('Enviar',1)); ?>

  <script type="text/javascript">
$('#newsletterNameField2').bind('blur',function(){if($('#newsletterNameField2').val().length == 0) $('#newsletterNameField2').val("Tu nombre");});
$('#newsletterNameField2').bind('click',function(){$('#newsletterNameField2').val("");});

$('#newsletterEmailField2').bind('blur',function(){if($('#newsletterEmailField2').val().length == 0) $('#newsletterEmailField2').val("Tu email");});
$('#newsletterEmailField2').bind('click',function(){$('#newsletterEmailField2').val("");});
</script>
