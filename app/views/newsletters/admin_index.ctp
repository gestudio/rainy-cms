<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Boletin de noticias',1));
?>
<h1><?=__('Emails dados de alta para newsletter',1);?></h1>



<p><?echo $paginator->counter(array(
        'format' => __('Pagina %page% de %pages%, 
                     mostrando %current% registros de un total de %count%, 
                     comenzando en el registro %start%, terminando en el %end%',1)
)); ?></p>
<p><?=$paginator->numbers()?></p>
  
  
  
    <table>
    <thead>
      <tr>
        <th><?=$paginator->sort(__('Correo electrónico',1), 'email')?></th>
        <th><?=$paginator->sort(__('Nombre',1), 'name')?></th>
        <th><?=$paginator->sort(__('Alta',1), 'created')?></th>
        <th><? __('opciones') ?></th>
      </tr>
    </thead>
    <tbody>
      <?
      foreach($Newsletters as $user){
        ?>
        <tr>
          <td><?=$user['Newsletter']['email']?></td>
          <td><?=$user['Newsletter']['name']?></td>
          <td><?=date('d-m-Y',$user['Newsletter']['created'])?></td>
          <td>
            <?=$html->link(
              __('Eliminar',true),
              array('action'=>'delete',$user['Newsletter']['id']),
              array('title'=>__('Eliminar',true)),
              str_replace('[[UserName]]',$user['Newsletter']['email'],__('¿Seguro que quieres borrar el usuario "[[UserName]]"?',true))
            )?>
          </td>
        </tr>
        <?
      }
      ?>
    </tbody>
  </table>
  
  
<p><?echo $paginator->counter(array(
        'format' => __('Pagina %page% de %pages%, 
                     mostrando %current% registros de un total de %count%, 
                     comenzando en el registro %start%, terminando en el %end%',1)
)); ?></p>
<p><?=$paginator->numbers()?></p>
  <hr />
<ul>
<li><?=$html->link(__('Insertar nuevo email',1),array('plugin'=>null, 'action'=>'admin_add'),array('class'=>'button'));?></li>
<li><?=$html->link(__('Insertar emails de forma masiva',1),array('plugin'=>null, 'action'=>'admin_add_masive'),array('class'=>'button'));?></li>
<li><?=$html->link(__('Crear nuevo boletín',1),array('plugin'=>null, 'action'=>'admin_newsletter'),array('class'=>'button'));?></li>
</ul>