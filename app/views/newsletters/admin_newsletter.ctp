<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Boletin de noticias',1),'/admin/newsletters');
$this->Html->addCrumb(__('Nuevo boletin',1));
?>
<h1 class="heading"><?__('Redactar nuevo email',1);?></h1>

<p><? __('Selecciona los grupos de destinatarios a los que quieres enviar la promoción.'); ?></p>


<?php


$grupos = array(__('Todos',1),__('Clientes',1),__('Administradores',1));
echo $form->create('Newsletter',array('url'=>array('action'=>'admin_newsletter')));

echo $form->input('group',array('label'=>__('Grupos',1),'options'=>$grupos));
echo $form->input('title',array('label'=>__('Título',1),'type'=>'text'));
echo $form->input('message',array('label'=>__('Mensaje',1),'type'=>'textarea','id'=>'newsletterMessage','style'=>'width:80%;height:400px;'));

echo $form->end(__('Previsualizar Mensaje',1));

?>

<?=$this->Html->script('jquery/wysiwyg/jquery.wysiwyg.pack.js',array('inline'=>false));?>
<?=$this->Html->css('/js/jquery/wysiwyg/jquery.wysiwyg.css','stylesheet',array('inline'=>false));?>

<script>
$('#newsletterMessage').wysiwyg({
    controls : {
        alertSep : { separator : true },
        alert : {
            visible   : true,
            exec      : function() { alert('Hello World'); },
            className : 'alert'
        }
    }
});
</script>