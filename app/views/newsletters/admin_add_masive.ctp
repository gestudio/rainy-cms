<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Boletin de noticias',1),'/admin/newsletters');
$this->Html->addCrumb(__('Inserción de usuarios',1));
?>
<h1><?=__('Añadir nuevo email para newsletter',1);?></h1>
<?=$form->create('Newsletter',array('url'=>array('action'=>'admin_add_masive')));?>
<?=$form->input('emails',array('label'=>__('Emails',1),'type'=>'textarea','after'=>__('Introducir direcciones de email separadas por comas o espacios.',1)));?>
<?=$form->end(__('Guardar',1));?>
