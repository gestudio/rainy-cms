<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Boletin de noticias',1),'/admin/newsletters');
$this->Html->addCrumb(__('Previsualizar el mensaje',1));
?>
<h1 class="heading"><?__('Previsualizar el mensaje');?></h1>

<p><? __('El boletín está listo para enviarse, repasa los datos y si son correctos, pulsa enviar boletín para ejecutar la acción.'); ?></p>

<dl>
	<dt><?__('Destinatarios');?></dt>
	<dd><?
	switch ($this->data['Newsletter']['group']){
		case 0:
			__('Todos');
			break;
		case 1:
			__('Clientes');
			break;
		case 2:
			__('Administradores');
			break;

	}
	echo ': '.$total;
	?></dd>
	<dt><?__('Título del mensaje');?></dt>
	<dd><?=$this->data['Newsletter']['title'];?></dd>
	<dt><?__('Mensaje');?></dt>
	<dd><?=$this->data['Newsletter']['message'];?></dd>
</dl>
<?php



echo $form->create('Newsletter',array('url'=>array('action'=>'admin_newsletter','confirmation'=>1)));

echo $form->input('group',array('type'=>'hidden'));
echo $form->input('title',array('type'=>'hidden'));
echo $form->input('message',array('type'=>'hidden'));

echo $form->end(__('Enviar Boletín.',1));
echo $html->link(__('Volver atrás',1),array('action'=>'admin_newsletter','email'=>serialize($this->data)));
?>