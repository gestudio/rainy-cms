<?php if (empty($promocion)):?>
 <p><strong><? __('Error Interno. No existe ninguna promoción activa.'); ?></strong></p>
<? else: ?>
<?=$this->Html->script('jquery/jquery.faded.js',array('inline'=>false));?>
<div id="slideshow">
	<ul style="position: relative;">
		 <? foreach ($promocion as $i => $promo): ?>
			<li title="<?=$promo['Promotion']['name']?>">
				<?
				if(!empty($promo['Promotion']['url'])) echo $this->Html->link($this->Html->image($promo['Promotion']['image'],array('alt'=>$promo['Promotion']['name'])),$promo['Promotion']['url'],array('class'=>'promo','escape'=>false));
				else echo $this->Html->image($promo['Promotion']['image'],array('alt'=>$promo['Promotion']['name']));
				
				echo $this->Html->tag('p',$promo['Promotion']['name']);
				?>
			</li>
		<? endforeach; ?>
	</ul>
	<div class="paging">
		<a href="#" class="prev">Ant.</a>
		<a href="#" class="next">Sig.</a>
	</div>
	<ul class="pagination">
		<?  foreach ($promocion as $i => $promo): ?>
			<li class="<?if($i == 0) echo 'current';?>"><a rel="<?=$i;?>" href="#"><?=$i+1;?></a></li>
		<? endforeach; ?>
	</ul>
</div>
<script type="text/javascript">$(function(){$("#slideshow").faded({speed:500,autoplay:5000});});</script>
<? endif; ?>