<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Promociones',1));
?><h1><?__('Promociones de portada');?></h1>

<table class="data" cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody>
      <tr>
		  <th></th>
        <th><?__('Promoci&oacute;n Seleccionada');?></th>
	<th><?__('Url');?></th>
        <th align="center"><? __('Fecha Creación');?></th>
        <th align="right"><?__('Acciones');?></th>
      </tr>

<?php if (count($arr_promocion) > 0) { ?>
<?php   foreach ($arr_promocion as $promocion) { ?>
      <tr>
	<td><?=$this->Html->image($promocion['Promotion']['image'],array('width'=>'80','height'=>'45'));?></td>
        <td>
          &nbsp;
          <strong><?php echo $promocion['Promotion']['name']; ?></strong>
        </td>
        <td class="dataTableContent">
          <?php echo $promocion['Promotion']['url']; ?>
        </td>
        <td class="dataTableContent" align="center">
          <?php echo ($promocion['Promotion']['created']); ?>
        </td>
        <td class="dataTableContent" align="right">
<?php     if (count($arr_promocion) > 1) { ?>
          <a href="<?php echo $html->url('/admin/promotions/index/deseleccionar/'.$promocion['Promotion']['id']); ?>" class="boton">
            Desactivar
          </a>
<?php     } ?>
          <a href="<?php echo $html->url('/admin/promotions/edit/'.$promocion['Promotion']['id']); ?>" class="boton">Editar</a>
        </td>
      </tr>
<?php   } ?>
<?php } else { ?>
      <tr>
        <td colspan="3">
          <?__('No hay ninguna promoción seleccionada.');?>
        </td>
      </tr>
<?php } ?>        

  </tbody>
</table>

<br/>
<br/>




<fieldset><legend><?__('Nueva Promoción');?></legend>
<? echo $this->Form->create('Promotion',array('url'=>array('action'=>'index')));?>
        <?php echo $form->input('Promotion.name', array('size' => '34', 'maxlength' => '32', 'label' => 'Nombre'))."\n"; ?>
        <?php echo $form->end(__('Guardar',1)); ?>
</fieldset>

<table class="data" cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody>
      <tr class="dataTableHeadingRow">
        <th><?__('Promoción');?></td>
        <th><?__('Url');?></td>
        <th align="center"><?__('Creación');?></td>
        <th align="right"></td>
      </tr>

<?php //Listado de Items
      foreach ($registros as $registro): 
?>
      <tr class="dataTableRow"
          onmouseover="rowOverEffect(this)"
          onmouseout="rowOutEffect(this)">
        <td class="dataTableContent">
          &nbsp;
          <strong><?php echo $registro['Promotion']['name']; ?></strong>
        </td>
        <td class="dataTableContent">
          <?php echo $registro['Promotion']['url']; ?>
        </td>
        <td class="dataTableContent" align="center">
          <?php echo $registro['Promotion']['created']; ?>
        </td>
        <td class="dataTableContent" align="right">
	    <?=$this->Html->link(__('Activar',1),'/admin/promotions/index/seleccionar/'.$registro['Promotion']['id'],array('class'=>'boton'));?>
	    <?=$this->Html->link(__('Editar',1),'/admin/promotions/edit/'.$registro['Promotion']['id'],array('class'=>'boton'));?>
	    <?=$this->Html->link(__('Eliminar',1),array('action'=>'index/borrar/', $registro['Promotion']['id']),array('class'=>'boton','confirm'=>__('¿Estás seguro de que quieres borrarla?',1)));?>
        </td>
      </tr>
<?php endforeach; ?>


  </tbody>
</table>

<br/>

