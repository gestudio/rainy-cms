<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Promociones de portada',1),'/admin/promotions');
$this->Html->addCrumb(__('Editar promoción',1));
?>
<h1><? __('Editar Promoción'); ?></h1>
<?php
  function cambiar_ampersand($coincidencia) {
    return str_replace('&', '&', $coincidencia[0]);
  }
  
  $cod_html = preg_replace_callback("/&[a-z|A-Z]+;/", "cambiar_ampersand", $this->data['Promotion']['html']);
?>
<div class="grid_16">
<?=$html->link(__('Volver al listado',1),array('plugin' => null, 'admin'=>true,'controller'=>'promotions','action'=>'index'),array('class'=>'button'),null, false)?>
</div>

<fieldset>
	<legend><? __('Editar promoción'); ?></legend>
  <?php 
  echo $form->create('Promotion',array('url'=>array('action'=>'admin_edit'), 'type'=>'file'));
  echo $form->hidden('Promotion.id'); 
  echo $form->input('Promotion.name', array('label' => 'Nombre')); 
  echo $form->input('Promotion.show_title',array('label'=>'Mostrar título','options'=>array('1'=>'Mostrar Titulo','0'=>'No Mostrar Titulo')));
  echo $form->input('Promotion.url', array('label' => 'URL'));
  
  ?>
  <h3><? __('Imagen Promoci&oacute;n:'); ?></h3>
  <?php 
      if ($this->data['Promotion']['image'] != '') {
	  $promocion = $this->data;
        if ($promocion['Promotion']['image'] != '') {

          if (trim($promocion['Promotion']['html']) != '') {
            $mapa = 'MapP'.sprintf("%05d",$promocion['Promotion']['id']).'';
            list($ancho, $altura, $tipo, $atr) = getimagesize(WWW_ROOT.$promocion['Promotion']['image']);
          }
  ?>        
	<img src="<?php echo $html->url($promocion['Promotion']['image']); ?>" alt="<? __('Imagen_Promocion'); ?>" width="900" border="0" <?php echo (trim($promocion['Promotion']['html']) != '')?('usemap="#'.$mapa.'" width="'.$ancho.'" height="'.$altura.'"'):''?>/>
  <?php
          if (trim($promocion['Promotion']['html']) != '') {
            echo '<map name="'.$mapa.'" id="'.$mapa.'">'."\n";
            
            $arr_html = split('<url>', trim($promocion['Promotion']['html']));
            //print_r($arr_html);
                      
            for ($contador = 0; $contador < count($arr_html); $contador++) {
              if (($contador % 2)  == 0) {
                echo $arr_html[$contador];
              } else {
                $item = split(':',$arr_html[$contador]);
                if ($item[0] == 'requestAction') {
                  echo $this->requestAction($item[1], array('return'));
                } else if ($item[0] == 'url'){
                  echo $html->url($item[1]);
                } else if ($item[0] == 'file'){
                  echo $html->url('/files/indexes/'.$item[1]);
                } else if ($item[0] == 'image'){
                  echo '<img src="'.$html->url('/img/'.$item[1]).'"/>';
                }
              }
            }
          
            //echo trim($promocion['Promotion']['html'])."\n";
            echo '</map>'."\n";
          }
        }
      }
?>
     <br />
     <?php echo $form->input('Promotion.fichero',array('type'=>'file'));?>
	 <h3><? __('Flash Promoci&oacute;n:'); ?></h3>
	 <?php 
	 
	 
	 
      if ($this->data['Promotion']['flash_file'] != '') {
        echo '<a href="'.$html->url($this->data['Promotion']['flash_file']).'" target="_blank" onclick="window.open(this.href,this.target);return false;">'."\n".
             'ver flash'."\n".
             '</a>'."\n".'<br/><br/>';
      }

      
	echo $form->input('Promotion.fichero_flash',array('type'=>'file'));
	echo $form->input('Promotion.flash_width', array('label' => 'Anchura del flash'));
	echo $form->input('Promotion.flash_height', array('label' => 'Alto del flash'));
	echo $form->input('Promotion.html', array('value' => $cod_html, 'label' => 'Mapa HTML'));
	echo $form->end('Actualizar');
	
	
	
	?>

</fieldset>

    <div class="infoBox"><h2><? __('Instrucciones'); ?></h2>
          <p>&Uacute;nicamente hay que poner los &quot;area shape's&quot;. El Tag &quot;map&quot; se pone autom&aacute;ticamente.
          <br/>
          El &aacute;rea HTML substituira al enlace guardado en URL de Navegaci&oacute;n.<br/>Para hacer referencia a la una direcci&oacute;n url se debe seguir el siguiente formato:
          <ul>
            <li>&lt;url&gt;url:/ruta_desde_webroot&lt;url&gt; para rutas relativas de cake.</li>
            <li>para una ruta absoluta simplemente la ponemos.</li>
          </ul>
          <br/>Para el fichero flash tenemos 2 alternativas:<br/>
          <ul>
          	<li>Tama&ntilde;o completo: 730x310</li>
          	<li>Con Banner: 730x*** (310 menos altura del banner)</li>
          </ul>
		  </p>
	</div>