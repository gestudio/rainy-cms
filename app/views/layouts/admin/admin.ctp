<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('SITE.TITLE'); ?></title>
		<?php
		echo $this->Html->script(array('jquery/jquery.min'));
		echo $this->Html->css(array(
			'reset',
			'960',
			'style',
			'admin',
			'/js/jquery/colorbox/colorbox.css'
		));
		echo $this->Html->script(array(
			'jquery/jquery.faded',
			'custom',
			'jquery/jquery.colorbox'
		));
		echo $scripts_for_layout;
		?>
		
	</head>



	<body>
		<div id="container">
			<div class="container_16">
				<!-- ======= Header logo ======= -->
				<div id="header" class="grid_16">
					<div class="grid_3">
						<a href="<?= $html->url('/admin/server'); ?>" title="<?php echo Configure::read('SITE.TAGLINE'); ?>"><img src="<?= $html->url('/img/logo_small.png'); ?>" id="" alt="<?php echo Configure::read('SITE.TAGLINE'); ?>" /></a>
					</div>
					<!-- ======= Header Social networking icons ======= -->
					<div class="grid_12">
						<?=$this->element('menu');?>
					</div>
				</div>
				<!-- ======= Footer area starts ======= -->
				<div class="account">
					<?__('Bienvenido/a');?> <strong><?=$this->Session->read('Auth.User.email');?></strong>. <?__('Último acceso');?>: <?=date('d/M',$this->Session->read('Auth.User.last_login'));?> a las <?=date('H:i',$this->Session->read('Auth.User.last_login'));?>.
				</div>
				<!-- ======= Footer area ends ======= -->
				<div class="grid_16"><?echo $this->Html->getCrumbs(' > ',__('Inicio',1));?></div>
				<div class="grid_16">
					<?php
					echo $this->Session->flash();
					echo $content_for_layout;
					?>
				</div>

				<!-- ======= Footer area starts ======= -->
				<div class="grid_16" id="footer">
				    <p><img src="<?= $html->url('/img/logo_small_dark.png'); ?>" id="" alt="<?php echo Configure::read('SITE.TAGLINE'); ?>" /></p>
				    <?=$this->element('admin/menu');?>
				</div>
				<!-- ======= Footer area ends ======= -->

			</div>
		</div>

		<?=$this->element('ga');?>
	</body>

</html>