<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('SITE.TITLE'); ?></title>
		<meta name="description" content="<?php echo Configure::read('SITE.TAGLINE'); ?>"/>
		<meta name="keywords" content="<?php echo Configure::read('SITE.KEYWORDS'); ?>"/>
		<?php
		echo $this->Html->script(array('jquery/jquery.min'));
		echo $this->Html->css(array(
			'reset',
			'960',
			'style',
			'/js/jquery/colorbox/colorbox.css'
		));
		echo $this->Html->script(array(
			'jquery/jquery.faded',
			'custom',
			'jquery/jquery.colorbox'
		));
		echo $scripts_for_layout;
		?>
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
		  {lang: 'es'}
		</script>
	</head>



	<body>

		<div class="container_16" id="content">
			<!-- ======= Header logo ======= -->
			<div id="header" class="grid_16">
				<a id="logo" href="<?= $html->url('/'); ?>" title="<?php echo Configure::read('SITE.TAGLINE'); ?>"><?php echo Configure::read('SITE.TITLE'); ?></a>
				<?=$this->element('menu');?>
			</div>
			<div id="subHeader">
			    <span class="navhome"><? $crumbs = $this->Html->getCrumbs(' - ',__('Inicio',1)); if (empty($crumbs)) echo __('Inicio').' - '.__('Bienvenido a ',1).Configure::read('SITE.TITLE');else echo $crumbs; ?></span>
			    <!-- ======= Header Social networking icons ======= -->
			    <ul id="social-icons">
				<li><a href="http://www.twitter.com/<?php echo Configure::read('SOCIAL.TWITTER'); ?>" target="_blank"><img src="<?= $html->url('/img/layout/twitter.png'); ?>" alt="Twitter" /></a></li>
				<li><a href="http://www.facebook.com/<?php echo Configure::read('SOCIAL.FACEBOOK'); ?>" target="_blank"><img src="<?= $html->url('/img/layout/facebook.png'); ?>" alt="Facebook" /></a></li>
				<li><?= $html->link($html->image('layout/rss.png', array('alt' => 'Email')), '/rss', array('escape' => false), false, false); ?></li>
			    </ul>
			</div>
			<div class="grid_16 mainContent">
				<?php
				echo $this->Session->flash();
				echo $content_for_layout;
				?>
			</div>

			<!-- ======= Footer area starts ======= -->
			<div class="grid_16" id="footer">
			    <?=$this->element('menu',array('noDropDowns'=>true));?>
			    <p><?=Configure::read('SITE.TITLE');?> &copy; <?=date("Y");?>, <? __('Todos los derechos reservados.');?></p>
			</div>
			<!-- ======= Footer area ends ======= -->

		</div>
<script type="text/javascript">
$('#emailsub').bind('blur',function(){if($('#emailsub').val().length == 0) $('#emailsub').val("Subscribirme a newsletter");});
$('#emailsub').bind('click',function(){$('#emailsub').val("");});
</script>
<?=$this->Js->writeBuffer();?>
		<?=$this->element('ga');?>
	</body>

</html>