<html>
	<head>
		<title><?=$title_for_layout?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body,table{margin:1%; font-family: Arial; font-size:14px;}
			.legal{font-size:80%; text-align: justify;}
			tr{height:10px;}
			h1{font-size:20px;}
			h2{font-size:18px;}
			
		</style>
	</head>
	<body onload="window.print(); window.close();">
	
		<?=$content_for_layout;?>
		
		
	</body>
</html>