<?php

header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

// force download dialog
//header("Content-type: application/vnd.ms-excel");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Transfer-Encoding: binary");
header("Content-Disposition: attachment; filename=".((isset($nombre_fichero))?$nombre_fichero:'EnviosCBH-').date('Ymd').".xls");

echo $content_for_layout;

?>