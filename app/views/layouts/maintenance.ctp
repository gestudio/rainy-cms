<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo Configure::read('SITE.TITLE'); ?> :: <?php echo $title_for_layout; ?></title>
<?php echo $html->charset(); ?>
<meta name="description" content="<?=Configure::read('SITE.TAGLINE');?>"/>
<meta name="keywords" content="<?=Configure::read('SITE.KEYWORDS');?>"/>
<meta name="robots" content="index, follow"/>
<link href='http://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet' type='text/css'>
<style>body{background:url(<? echo $this->Html->url('/img/layout/bg.png');?>);padding:200px 35%;font-size:70%;font-family:Verdana,sans-serif}h1{color:#595959;text-shadow:0 1px 0 #fff;font-family:"Yellowtail",cursive}p{}</style>
</head>
<body>
<img src="<? echo $this->Html->url('/img/logo.png');?>" alt="<?=Configure::read('SITE.TITLE');?>" align="center"/>
<h1><?__('Página web en mantenimiento');?></h1>
<p><strong><?__('Teléfono:');?></strong> <?=Configure::read('SITE.PHONE');?><br />
<strong><?__('Email de contacto:');?></strong><br /><?=Configure::read('SITE.ADMINISTRATOR');?>
</p>

</body>
</html>
<?php Configure::write('debug', 0); ?>