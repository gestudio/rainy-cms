<?php echo (isset($html_titulo))?$html_titulo:Configure::read('SITE.TITLE'); ?>
=================================================================================


    <?php echo $content_for_layout; ?>
    

		
=================================================================================		
		
		
		
		
		
<?=Configure::read('SITE.TITLE');?>  &copy; <?=date('Y');?>  http://www.<?=$_SERVER['SERVER_NAME'];?>


Al pulsar en el enlace acepto las siguientes condiciones:
=================================================================================
A efecto de lo previsto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, <?=Configure::read('SITE.TITLE');?> informa al Usuario de la existencia de un fichero automatizado de datos de carácter personal creado por y para <?=Configure::read('SITE.TITLE');?> y bajo su responsabilidad, con la finalidad de realizar el mantenimiento y gestión de la relación con el Usuario, así como las labores de información. Ud. da su consentimiento expreso para la inclusión de sus datos en el mencionado fichero y la remisión de informaciones comerciales de productos de <?=Configure::read('SITE.TITLE');?> por cualquier medio, incluido el correo electrónico o SMS. <?=Configure::read('SITE.TITLE');?> podrá realizar una cesión de los datos de carácter personal objeto de tratamiento automatizado a cualesquiera personas físicas o jurídicas con las cuales mantenga acuerdos de colaboración. En cualquier caso, podrá ejercitar gratuitamente los derechos de acceso, rectificación, cancelación y oposición dirigiéndose a a este remitente.