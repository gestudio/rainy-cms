<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo (isset($html_titulo))?$html_titulo:Configure::read('SITE.TITLE'); ?></title>
<style type="text/css">
<!--
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: x-small;
	color: #000000;
}
a:link {
	color: #333333;
	font-weight: bold;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #333333;
}
a:hover {
	text-decoration: none;
	color: #000000;
}
a:active {
	text-decoration: none;
	color: #333333;
}
h1{ font-family: "Times New Roman", Times, serif; font-size:23px; color:#333333; margin-bottom:0em;}
h2{ text-transform:uppercase; font-family:"Times New Roman", Times, serif; font-size:12px; color:#666666; margin-top:0em;}
.style1 {
	color: #CCCCCC
}
.LOPD{color: #666666; font-family:"Times New Roman", Times, serif; text-align:justify;}
-->
</style>
</head>



<body>
	<img src="http://<?=$_SERVER['SERVER_NAME'];?>/<?=$this->Html->url('/img/logo.png');?>" alt="Logo <?=Configure::read('SITE.TITLE');?>" width="350" height="74" />
<table width="790" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    
    <?php echo $content_for_layout; ?>
    
    </td>
  </tr>
  
  <tr>
    <td height="54"><table width="790" border="0" cellspacing="0" cellpadding="0">
      <tr>

        <td><hr /><strong><?=Configure::read('SITE.TITLE');?></strong> &copy; <?=date('Y');?> <a href="http://<?=$_SERVER['SERVER_NAME'];?>" target="_blank">http://<?=$_SERVER['SERVER_NAME'];?></a><br />        </td>

      </tr>
    </table>
    </td>
  </tr>
  <tr><td>
		  <p>Si deseas dar de baja tu dirección <?=$email?> <a href="http://<?=$_SERVER['SERVER_NAME']?><?=$this->Html->url('/newsletters/erase/'.base64_encode($email).DS.  md5($email));?>" title="Darme de baja">clicka aquí</a>.</p>
		  <div class="lopd"><strong>Al pulsar en el enlace acepto las siguientes condiciones:</strong> A efecto de lo previsto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, <?=Configure::read('SITE.TITLE');?> informa al Usuario de la existencia de un fichero automatizado de datos de carácter personal creado por y para <?=Configure::read('SITE.TITLE');?> y bajo su responsabilidad, con la finalidad de realizar el mantenimiento y gestión de la relación con el Usuario, así como las labores de información. Ud. da su consentimiento expreso para la inclusión de sus datos en el mencionado fichero y la remisión de informaciones comerciales de productos de <?=Configure::read('SITE.TITLE');?> por cualquier medio, incluido el correo electrónico o SMS. <?=Configure::read('SITE.TITLE');?> podrá realizar una cesión de los datos de carácter personal objeto de tratamiento automatizado a cualesquiera personas físicas o jurídicas con las cuales mantenga acuerdos de colaboración. En cualquier caso, podrá ejercitar gratuitamente los derechos de acceso, rectificación, cancelación y oposición dirigiéndose a a este remitente.</div></td></tr>
</table>
	
</body>
</HTML>