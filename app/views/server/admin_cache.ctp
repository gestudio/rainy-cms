<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Cache de vistas',1));
?>
<h1><? __('Eliminar cache de vistas.'); ?></h1>
<?
echo $form->create('cache',array('url'=>array('controller'=>'server','action'=>'admin_cache')));
echo $html->tag('p',__('Al eliminar la cache se borrarán los archivos con datos antiguos de la parte pública para incluir nuevas actualizaciones. Los cambios en base de datos deberian actualizarse sin necesidad de eliminar la cache.',1));
echo $html->tag('p',__('Únicamente se eliminarán elementos cache de Categorias, Noticias y Productos.',1));
echo $form->input('proceed',array('type'=>'hidden','value'=>1));
echo $form->end(__('Eliminar ahora.',1));

?>
