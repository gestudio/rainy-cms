<?
$this->Html->addCrumb(__('Extranet',1), '/admin/server');
$this->Html->addCrumb(__('Estado del servidor',1));
?>
<h1><?__('Estado actual del servidor');?></h1>

<pre>
<p><?__('Tiempo online');?>: 
<?php system("uptime"); ?>
</p>
<p><?__('Información del sistema');?>:
<?php system("uname -a"); ?>
</p>

<p><?__('Uso de la memoria en MB');?>
<?php system("free -m"); ?>
</p>

<p><?__('Uso del disco'); ?>
<?php system("df -h"); ?>
</p> 

<b><?__('Información sobre la CPU');?></b> 
<?php system("cat /proc/cpuinfo | grep \"model name\\|processor\""); ?>
</pre>


<h2><?=$this->Html->link(__('Información sobre PHP',1),array('action'=>'phpinfo'));?></h2>