<?
$this->Html->addCrumb(__('Extranet',1));
?>
<h1><?=String::insert(__('Bienvenido a la administración de :sitio.',1),array('sitio'=>Configure::read('SITE.TITLE')));?></h1>


<div class="adminIcons">
    <?=$this->Html->image('admin/index/contacts.png');?>
    <h2><?=$this->Html->link(__('Agenda de contactos',1),array('controller'=>'users','action'=>'index'));?></h2>
    <p><?__('Consulta los usuarios y clientes almacenados.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/events.png');?>
    <h2><?=$this->Html->link(__('Eventos',1),array('controller'=>'events','action'=>'index'));?></h2>
    <p><?__('Da de alta nuevos eventos de interes público que serán visibles por cualquier visitante.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/documents.png');?>
    <h2><?=$this->Html->link(__('Documentos',1),array('controller'=>'documents','action'=>'index'));?></h2>
    <p><?__('Da de alta documentación y compartela con uno o más contactos.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/photos.png');?>
    <h2><?=$this->Html->link(__('Fotos',1),array('controller'=>'photos','action'=>'index'));?></h2>
    <p><?__('Edita, añade o elimina fotos de la galería de fotos pública.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/newsletter.png');?>
    <h2><?=$this->Html->link(__('Newsletters',1),array('controller'=>'newsletters','action'=>'index'));?></h2>
    <p><?__('Envía boletines por correo electrónico a tus clientes de forma cómoda y rápida.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/server.png');?>
    <h2><?=$this->Html->link(__('Servidor',1),array('controller'=>'server','action'=>'state'));?></h2>
    <p><?__('Consulta el estado del almacenamiento y funcionamiento del servidor.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/promotions.png');?>
    <h2><?=$this->Html->link(__('Promociones',1),array('controller'=>'promotions','action'=>'index'));?></h2>
    <p><?__('Administra y crea nuevas promociones de la página principal.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/logs.png');?>
    <h2><?=$this->Html->link(__('Registro de eventos',1),array('controller'=>'server','action'=>'logs'));?></h2>
    <p><?__('Información sobre eventos, errores y advertencias del sistema.');?></p>
</div>

<div class="adminIcons">
    <?=$this->Html->image('admin/index/checklist.gif');?>
    <h2><?=$this->Html->link(__('Configuración',1),array('controller'=>'settings','action'=>'index'));?></h2>
    <p><?__('Configura los distintos parámetros de la página web, como títulos, emails o descripciones...');?></p>
</div>
