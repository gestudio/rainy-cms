<?
$this->set('title_for_layout',Configure::read('SITE.SLOGAN'));
//$this->Html->addCrumb('Users', '/users');
?>
<div class="indexHeadings">
    <h1><?=Configure::read('SITE.TAGLINE');?></h1>
    <p><?=Configure::read('SITE.SLOGAN');?></p>
</div>
<?=$this->requestAction('/promotions/index',array('return'));?>

<div class="indexBox">
    <div class="grid_5 alpha">
	<?=$this->Html->image('layout/index1.gif');?>
	<h2><?__('Rápida comunicación');?></h2>
	<p><?__('Trámitamos online todas tus dudas y gestiones, además te enviamos la documentación por email para que no tardes ni un minuto más.');?></p>
    </div>
    <div class="grid_5">
	<?=$this->Html->image('layout/index2.gif');?>
	<h2><?__('Maletín de documentos');?></h2>
	<p><?__('Todos los documentos se guardan online de forma segura en nuestro servidor y podrás acceder siempre que quieras con tu contraseña.');?></p>
    </div>
    <div class="grid_5 omega">
	<?=$this->Html->image('layout/index3.gif');?>
	<h2><?__('Integrado con Twitter');?></h2>
	<p><?__('Nuestras publicaciones de Twitter se reflejan en la sección noticias para que estes informado sobre los últimos eventos que publicamos.');?></p>
    </div>
</div>