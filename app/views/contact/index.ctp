<div id="ajaxupdateable">
<? $this->set('title_for_layout','Contacta con nosotros');?>
<? $this->Html->addCrumb(__('Contactanos',1)); ?>
<div class="grid_4 alpha" style="padding-top:140px">
<?=$this->Html->image('layout/businessman.jpg');?>
</div>
<div class="grid_12 omega">
<?php if (isset($name) && !empty($name)) { //If email is sent ?>
	<h1><? __('Mensaje enviado correctamente'); ?></h1>
	<p><?= String::insert(__('Gracias :name por usar nuestro formulario de contacto. ¡Pronto nos pondremos en contacto contigo!', true), array('name' => $name)); ?></p>
<?php 

} else {
	?>
<h1><? __('Contacta con nosotros.'); ?></h1>
<p>Rellena los campos del formulario y pronto nos pondremos en contacto contigo, gracias.</p>

<?

	if (isset($hasError)) { //If errors are found ?>
		<p class="error"><? __('Comprueba que hayas rellenado correctamente los campos de contacto.'); ?></p>
	<?php
	}


	echo $form->create('Contact', array('url' => '/contact/index'));
	echo $form->input('name', array('label' => __('Nombre', 1)));
	echo $form->input('phone', array('label' => __('Teléfono', 1)));
	echo $form->input('email', array('label' => __('Correo electrónico', 1)));
	echo $form->input('details', array('label' => __('Mensaje', 1)));
	echo $form->input('lopd', array('type'=>'checkbox','label' => __('He leido y acepto la ', 1).' '.$this->Html->link(__('LOPD',1),'/info/legal')));
	echo $this->Js->submit(__('Enviar', 1),array('update'=>'#ajaxupdateable'));
	echo $form->end();
}
?>
</div>
    </div>