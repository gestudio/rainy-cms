<?php
/* Blog Test cases generated on: 2011-10-19 19:13:33 : 1319051613*/
App::import('Model', 'Blog');

class BlogTestCase extends CakeTestCase {
	var $fixtures = array('app.blog', 'app.user', 'app.user_group', 'app.user_detail', 'app.document', 'app.documents_user', 'app.photo', 'app.category', 'app.product', 'app.billing', 'app.tax', 'app.categories_product', 'app.categories_blog');

	function startTest() {
		$this->Blog =& ClassRegistry::init('Blog');
	}

	function endTest() {
		unset($this->Blog);
		ClassRegistry::flush();
	}

}
