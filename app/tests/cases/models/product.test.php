<?php
/* Product Test cases generated on: 2011-10-14 16:48:24 : 1318610904*/
App::import('Model', 'Product');

class ProductTestCase extends CakeTestCase {
	var $fixtures = array('app.product', 'app.billing', 'app.tax', 'app.category', 'app.categories_product');

	function startTest() {
		$this->Product =& ClassRegistry::init('Product');
	}

	function endTest() {
		unset($this->Product);
		ClassRegistry::flush();
	}

}
