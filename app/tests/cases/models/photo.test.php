<?php
/* Photo Test cases generated on: 2011-10-13 19:02:49 : 1318532569*/
App::import('Model', 'Photo');

class PhotoTestCase extends CakeTestCase {
	var $fixtures = array('app.photo');

	function startTest() {
		$this->Photo =& ClassRegistry::init('Photo');
	}

	function endTest() {
		unset($this->Photo);
		ClassRegistry::flush();
	}

}
