<?php
/* Document Test cases generated on: 2011-10-11 20:51:07 : 1318366267*/
App::import('Model', 'Document');

class DocumentTestCase extends CakeTestCase {
	var $fixtures = array('app.document', 'app.user', 'app.user_group', 'app.documents_user');

	function startTest() {
		$this->Document =& ClassRegistry::init('Document');
	}

	function endTest() {
		unset($this->Document);
		ClassRegistry::flush();
	}

}
