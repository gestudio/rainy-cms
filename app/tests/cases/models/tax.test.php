<?php
/* Tax Test cases generated on: 2011-10-14 16:49:44 : 1318610984*/
App::import('Model', 'Tax');

class TaxTestCase extends CakeTestCase {
	var $fixtures = array('app.tax', 'app.product', 'app.billing', 'app.category', 'app.categories_product');

	function startTest() {
		$this->Tax =& ClassRegistry::init('Tax');
	}

	function endTest() {
		unset($this->Tax);
		ClassRegistry::flush();
	}

}
