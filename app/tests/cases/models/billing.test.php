<?php
/* Billing Test cases generated on: 2011-10-14 16:46:00 : 1318610760*/
App::import('Model', 'Billing');

class BillingTestCase extends CakeTestCase {
	var $fixtures = array('app.billing', 'app.product');

	function startTest() {
		$this->Billing =& ClassRegistry::init('Billing');
	}

	function endTest() {
		unset($this->Billing);
		ClassRegistry::flush();
	}

}
