<?php
/* UserDetail Test cases generated on: 2011-10-13 11:46:44 : 1318506404*/
App::import('Model', 'UserDetail');

class UserDetailTestCase extends CakeTestCase {
	var $fixtures = array('app.user_detail', 'app.user', 'app.user_group');

	function startTest() {
		$this->UserDetail =& ClassRegistry::init('UserDetail');
	}

	function endTest() {
		unset($this->UserDetail);
		ClassRegistry::flush();
	}

}
