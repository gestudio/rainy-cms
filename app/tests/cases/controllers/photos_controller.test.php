<?php
/* Photos Test cases generated on: 2011-10-13 19:03:46 : 1318532626*/
App::import('Controller', 'Photos');

class TestPhotosController extends PhotosController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PhotosControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.photo');

	function startTest() {
		$this->Photos =& new TestPhotosController();
		$this->Photos->constructClasses();
	}

	function endTest() {
		unset($this->Photos);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

}
