<?php
/* Taxes Test cases generated on: 2011-10-14 16:51:21 : 1318611081*/
App::import('Controller', 'Taxes');

class TestTaxesController extends TaxesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TaxesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.tax', 'app.product', 'app.billing', 'app.category', 'app.categories_product');

	function startTest() {
		$this->Taxes =& new TestTaxesController();
		$this->Taxes->constructClasses();
	}

	function endTest() {
		unset($this->Taxes);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

}
