<?php
/* Blogs Test cases generated on: 2011-10-19 19:15:31 : 1319051731*/
App::import('Controller', 'Blogs');

class TestBlogsController extends BlogsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BlogsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.blog', 'app.user', 'app.user_group', 'app.user_detail', 'app.document', 'app.documents_user', 'app.photo', 'app.category', 'app.product', 'app.billing', 'app.tax', 'app.categories_product', 'app.categories_blog');

	function startTest() {
		$this->Blogs =& new TestBlogsController();
		$this->Blogs->constructClasses();
	}

	function endTest() {
		unset($this->Blogs);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

}
