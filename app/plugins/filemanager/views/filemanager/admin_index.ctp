
<h2><?php echo $title_for_layout; ?></h2>

<div class="actionsBox">
	<?php echo $filemanager->link(__('Subir fichero', true), array('controller' => 'filemanager', 'action' => 'upload'), $path, null,'important'); ?>
	<?php echo $filemanager->link(__('Nueva carpeta', true), array('controller' => 'filemanager', 'action' => 'create_directory'), $path, null,'important'); ?>
	<?php echo $filemanager->link(__('Nuevo archivo', true), array('controller' => 'filemanager', 'action' => 'create_file'), $path, null,'important'); ?>
</div>

<div class="breadcrumb">
	<?php
	echo __('Estás aquí:', true) . ' ';
	$breadcrumb = $filemanager->breadcrumb($path);
	foreach ($breadcrumb AS $pathname => $p) {
		echo $filemanager->linkDirectory($pathname, $p);
		echo DS;
	}
	?>
</div>

<div class="directory-content">
	<table cellpadding="0" cellspacing="0">
		<?php
		$tableHeaders = $html->tableHeaders(array(
					'',
					__('Directory content', true),
					__('Actions', true),
				));
		echo $tableHeaders;

		// directories
		$rows = array();
		foreach ($content['0'] AS $directory) {
			$actions = $filemanager->linkDirectory(__('Abrir', true), $path . $directory . DS);

			$actions .= ' ' . $filemanager->link(__('Eliminar', true), array(
						'controller' => 'filemanager',
						'action' => 'delete_directory',
						'token' => $this->params['_Token']['key'],
							), $path . $directory);

			$rows[] = array(
				$html->image('/img/icons/folder.png'),
				$filemanager->linkDirectory($directory, $path . $directory . DS),
				$actions,
			);
		}
		echo $html->tableCells($rows, array('class' => 'directory'), array('class' => 'directory'));

		// files
		$rows = array();
		foreach ($content['1'] AS $file) {
			$actions = $filemanager->link(__('Editar', true), array('controller' => 'filemanager', 'action' => 'admin_edit'), $path . $file);

			$actions .= ' ' . $filemanager->link(__('Eliminar', true), array(
						'controller' => 'filemanager',
						'action' => 'delete_file',
						'token' => $this->params['_Token']['key'],
							), $path . $file);

			$rows[] = array(
				$html->image('/img/icons/' . $filemanager->filename2icon($file)),
				$filemanager->linkFile($file, $path . $file),
				$actions,
			);
		}
		echo $html->tableCells($rows, array('class' => 'file'), array('class' => 'file'));

		echo $tableHeaders;
		?>
	</table>
</div>