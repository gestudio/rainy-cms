<?= $this->Html->script('/filemanager/js/codemirror.js', array('inline' => true)); ?>
<?= $this->Html->css('/filemanager/css/codemirror.css', 'stylesheet'); ?>
<?= $this->Html->script('/filemanager/js/xml.js', array('inline' => true)); ?>
<?= $this->Html->script('/filemanager/js/javascript.js', array('inline' => true)); ?>
<?= $this->Html->script('/filemanager/js/css.js', array('inline' => true)); ?>
<?= $this->Html->script('/filemanager/js/clike.js', array('inline' => true)); ?>
<?= $this->Html->script('/filemanager/js/php.js', array('inline' => true)); ?>
<?= $this->Html->css('/filemanager/css/cobalt.css', 'stylesheet'); ?>


<style type="text/css">
	.CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black;text-shadow: none !important;}
	.fullscreen {
		display: block;
		position: absolute;
		top: 0;
		left: 0;
		font-size:120%;
		width: 100%;
		height: 100%;
		z-index: 1;
		margin: 0;
		padding: 0;
		border: 0px solid #BBBBBB;
		opacity: 1;
	}
	.CodeMirror-lines{text-shadow:0px 0px 0px !important;}
	div#inner{position:static !important;}
</style>

<h2><?php echo $title_for_layout; ?></h2>

<div class="breadcrumb">
	<?php
	echo __('Estás aquí:', true) . ' ';
	$breadcrumb = $filemanager->breadcrumb($path);
	foreach ($breadcrumb AS $pathname => $p) {
		echo $filemanager->linkDirectory($pathname, $p);
		echo DS;
	}
	?>
</div>
<hr />
<?
echo $form->create('Filemanager', array(
	'url' => $html->url(array(
		'controller' => 'filemanager',
		'action' => 'admin_edit',
			), true) . '?path=' . urlencode($absolutefilepath),
));
echo $this->Form->input('file', array('label' => __('Editar archivo', 1), 'type' => 'textarea', 'value' => ($content), 'class' => 'fulls', 'style' => 'width:95%;height:350px;background:#efefef;'));
?>
<hr />
<?
echo $this->Html->tag('p', $this->Html->link(__('Cancelar', 1), array('action' => 'admin_index'), array('class' => 'important')), array('class' => 'grid_2'));
echo $this->Form->submit(__('Save', 1));

echo $this->Form->end();
?>
<hr />
<h5>Para ver a pantalla completa presionar la tecla ESC</h5>
<script>

	(function () {

		var editor = CodeMirror.fromTextArea(document.getElementById("FilemanagerFile"), {
			lineNumbers: true,
			matchBrackets: true,
			mode: "application/x-httpd-php",
			indentUnit: 8,
			indentWithTabs: true,
			enterMode: "keep",
			tabMode: "shift",
			theme: "cobalt",
			onKeyEvent: function(i, e) {
				// Hook into F11
				if ((e.keyCode == 122 || e.keyCode == 27) && e.type == 'keydown') {
					e.stop();
					return toggleFullscreenEditing();
				}
			}
		});

		function toggleFullscreenEditing()
		{
			var editorDiv = $j('.CodeMirror-scroll');
			if (!editorDiv.hasClass('fullscreen')) {
				toggleFullscreenEditing.beforeFullscreen = { height: editorDiv.height(), width: editorDiv.width() }
				editorDiv.addClass('fullscreen');
				editorDiv.height('100%');
				editorDiv.width('100%');
				editor.refresh();
			}
			else {
				editorDiv.removeClass('fullscreen');
				editorDiv.height(toggleFullscreenEditing.beforeFullscreen.height);
				editorDiv.width(toggleFullscreenEditing.beforeFullscreen.width);
				editor.refresh();
			}
		}

	})();
</script>