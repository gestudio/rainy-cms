<?php

class FilemanagerController extends FilemanagerAppController {

	var $uses = array();
	var $helpers = array('Filemanager.Filemanager');
	public $deletablePaths = array();

	
	/*
	 * Function beforeFilter
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();

		$this->deletablePaths = array(
			WWW_ROOT,
		);
		$this->set('deletablePaths', $this->deletablePaths);
		
		App::import('Core', 'File');
	}

	/*
	 * Function admin_index
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	function admin_index() {

		$this->folder = new Folder;

		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$path = APP;
		}

		$this->set('title_for_layout', __('File Manager', true));
		$this->folder->path = $path;

		$content = $this->folder->read();
		$this->set(compact('content', 'path'));
	}

	/*
	 * Function admin_edit
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_edit() {
		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
			$absolutefilepath = $path;
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}
		

		$path_e = explode(DS, $path);
		$n = count($path_e) - 1;
		$filename = $path_e[$n];
		unset($path_e[$n]);
		$path = implode(DS, $path_e);
		$this->file = new File($absolutefilepath, true);
		$this->set('title_for_layout', sprintf(__('Edit file: %s', true), $filename));
		
		if (!empty($this->data)) {
			if ($this->file->write($this->data['Filemanager']['file'])) {
				$this->Session->setFlash(__('File saved successfully', true), 'default', array('class' => 'success'));
			}
		}

		$content = $this->file->read();

		$this->set(compact('content', 'path', 'absolutefilepath'));
	}

	
	/*
	 * Function admin_upload
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */
	public function admin_upload() {
		$this->set('title_for_layout', __('Upload', true));

		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$path = APP;
		}
		$this->set(compact('path'));

		if (isset($this->data['Filemanager']['file']['tmp_name']) &&
				is_uploaded_file($this->data['Filemanager']['file']['tmp_name'])) {
			$destination = $path . $this->data['Filemanager']['file']['name'];
			move_uploaded_file($this->data['Filemanager']['file']['tmp_name'], $destination);
			$this->Session->setFlash(__('File uploaded successfully.', true), 'default', array('class' => 'success'));
			$redirectUrl = Router::url(array('controller' => 'filemanager', 'action' => 'admin_index'), true) . '?path=' . urlencode($path);

			$this->redirect($redirectUrl);
		}
	}

	/*
	 * Function admin_delete
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_delete() {
		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}

		if (!isset($this->params['named']['token']) || ($this->params['named']['token'] != $this->params['_Token']['key'])) {
			$blackHoleCallback = $this->Security->blackHoleCallback;
			$this->$blackHoleCallback();
		}

		if (file_exists($path) && unlink($path)) {
			$this->Session->setFlash(__('File deleted', true), 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('An error occured, folders must be empty first.', true), 'default', array('class' => 'error'));
		}

		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->redirect(array('action' => 'index'));
		}

		exit();
	}

	/*
	 * Function 
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_delete_file() {
		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'admin_index'));
		}

		if (!isset($this->params['named']['token']) || ($this->params['named']['token'] != $this->params['_Token']['key'])) {
			$blackHoleCallback = $this->Security->blackHoleCallback;
			$this->$blackHoleCallback();
		}

		if (file_exists($path) && unlink($path)) {
			$this->Session->setFlash(__('File deleted', true), 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('An error occured', true), 'default', array('class' => 'error'));
		}

		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}

		exit();
	}

	/*
	 * Function 
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_delete_directory() {
		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'admin_index'));
		}

		if (!isset($this->params['named']['token']) || ($this->params['named']['token'] != $this->params['_Token']['key'])) {
			$blackHoleCallback = $this->Security->blackHoleCallback;
			$this->$blackHoleCallback();
		}

		if (is_dir($path) && rmdir($path)) {
			$this->Session->setFlash(__('Directory deleted', true), 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('An error occured', true), 'default', array('class' => 'error'));
		}

		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}

		exit();
	}

	/*
	 * Function admin_rename
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_rename() {
		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'admin_index'));
		}

		if (isset($this->params['url']['newpath'])) {
			// rename here
		}

		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}
	}

	
	/*
	 * Function admin_create_directory
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */
	public function admin_create_directory() {
		$this->set('title_for_layout', __('New Directory', true));

		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}

		if (!empty($this->data)) {
			$this->folder = new Folder;
			if ($this->folder->create($path . $this->data['Filemanager']['name'])) {
				$this->Session->setFlash(__('Directory created successfully.', true), 'default', array('class' => 'success'));
				$redirectUrl = Router::url(array('controller' => 'filemanager', 'action' => 'index'), true) . '?path=' . urlencode($path);
				$this->redirect($redirectUrl);
			} else {
				$this->Session->setFlash(__('An error occured', true), 'default', array('class' => 'error'));
			}
		}

		$this->set(compact('path'));
	}

	/*
	 * Function admin_create_file
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_create_file() {
		$this->set('title_for_layout', __('New File', true));

		if (isset($this->params['url']['path'])) {
			$path = $this->params['url']['path'];
		} else {
			$this->redirect(array('controller' => 'filemanager', 'action' => 'index'));
		}

		if (!empty($this->data)) {
			if (touch($path . $this->data['Filemanager']['name'])) {
				$this->Session->setFlash(__('File created successfully.', true), 'default', array('class' => 'success'));
				$redirectUrl = Router::url(array('controller' => 'filemanager', 'action' => 'admin_index'), true) . '?path=' . urlencode($path);
				$this->redirect($redirectUrl);
			} else {
				$this->Session->setFlash(__('An error occured', true), 'default', array('class' => 'error'));
			}
		}

		$this->set(compact('path'));
	}

	/*
	 * Function admin_chmod
	 * 
	 * @description 
	 * @param 
	 * @return 
	 *
	 */

	public function admin_chmod() {
		
	}

}

?>