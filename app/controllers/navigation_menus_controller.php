<?php

class NavigationMenusController extends AppController {

	var $name = 'NavigationMenus';
	
	
	
	/*
	 * Function getMenu
	 *
	 * @param 
	 * @description 
	 * @return 
	 * @access 
	 *
	 */
	function getMenu(){
		if($this->Session->check('Auth.User.id')){
			$dom = $this->NavigationMenu->find('threaded',array(
				'conditions'=>array('user_groups LIKE' => '%|'.$this->Auth->User('user_group_id').'|%'),
				'order'=>'lft asc'
				));
		}else{
			$dom = $this->NavigationMenu->find('threaded',array(
				'conditions'=>array('user_groups LIKE' => '%|0|%'),
				'order'=>'lft asc'
				));
		}
		
		return $dom;
	}
	
	
	
	function admin_index() {
		$menu= $this->NavigationMenu->generatetreelist(null, null, null, "  + + ");
		$this->set(compact('menu'));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$userGroups = '|';
			if(!empty($this->data['NavigationMenu']['user_groups'])){
				foreach($this->data['NavigationMenu']['user_groups'] as $group)
						$userGroups .= $group.'|';
			}
			$this->data['NavigationMenu']['user_groups']= $userGroups;
			
			$this->NavigationMenu->save($this->data);
			$this->redirect(array('action' => 'index'));
		} else {
			$parents[0] = __('No Parent',1);
			$this->loadModel('UserGroup');
			
			$groups = $this->UserGroup->find('list');
			array_unshift($groups, __('No authentified',1));
					
			$nodelist = $this->NavigationMenu->generatetreelist(null, null, null, " - ");
			if ($nodelist)
				foreach ($nodelist as $key => $value)
					$parents[$key] = $value;
			$this->set(compact('parents','groups'));
		}
	}

	function admin_edit($id=null) {
		if (!empty($this->data)) {
			
			$userGroups = '|';
			if(!empty($this->data['NavigationMenu']['user_groups'])){
				foreach($this->data['NavigationMenu']['user_groups'] as $group)
						$userGroups .= $group.'|';
			}
			
			$this->data['NavigationMenu']['user_groups']= $userGroups;
			
			if ($this->NavigationMenu->save($this->data) == false)
				$this->Session->setFlash('Error saving Node.');
			$this->redirect(array('action' => 'index'));
			
		} else {
			
			$this->data = $this->NavigationMenu->read(null, $id);
			
			if(empty($this->data)) die("No ID received");
			$this->data['NavigationMenu']['user_groups'] = preg_split("/[\s|]+/", $this->data['NavigationMenu']['user_groups'],-1, PREG_SPLIT_NO_EMPTY);
			
			$this->loadModel('UserGroup');
			$groups = $this->UserGroup->find('list');
			array_unshift($groups, __('No authentified',1));
			
			$parents[0] = __('No Parent',1);
			
			$nodelist = $this->NavigationMenu->generatetreelist(null, null, null, " + ");
			if ($nodelist)
				foreach ($nodelist as $key => $value)
					$parents[$key] = $value;
			$this->set(compact('parents','groups'));
		}
	}

	function admin_delete($id=null) {
		if ($id == null)
			die("No ID received");
		$this->NavigationMenu->id = $id;
		if ($this->NavigationMenu->delete() == false)
			$this->Session->setFlash('The item could not be deleted.');
		$this->redirect(array('action' => 'index'));
	}

	function admin_moveup($id=null) {
		if ($id == null)
			die("No ID received");
		$this->NavigationMenu->id = $id;
		if ($this->NavigationMenu->moveup() == false)
			$this->Session->setFlash('The item could not be moved up.');
		$this->redirect(array('action' => 'index'));
	}

	function admin_movedown($id=null) {
		if ($id == null)
			die("No ID received");
		$this->NavigationMenu->id = $id;
		if ($this->NavigationMenu->movedown() == false)
			$this->Session->setFlash('The Node could not be moved down.');
		$this->redirect(array('action' => 'index'));
	}

}

?>
