<?php
class PhotosController extends AppController {

	var $name = 'Photos';
	var $helpers = array('Text');

	function index() {
	    $this->set('title_for_layout',__('Galería de fotos',1));
		$this->Photo->recursive = 0;
		$this->paginate = array('limit'=>18,'order'=>'id desc');
		$this->set('photos', $this->paginate());
	}

	function view($id = null) {
	    $this->set('title_for_layout',__('Galería de fotos',1));
		if (!$id) {
			$this->Session->setFlash(__('Foto no encontrada', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('photo', $this->Photo->read(null, $id));
	}

	
	
	
	function admin_index() {
		$this->paginate = array('limit'=>10,'order'=>'id desc');
		$this->Photo->recursive = 0;
		$this->set('photos', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
		    
		    
		    $filename = microtime(true);
		    
		    $fileUpload = $this->__upload_file($this->data['Photo']['file'], 'img/uploads', $filename, 10240000,array('image/jpg','image/jpeg','image/pjpeg','image/png','image/gif'));
		    
		    $filename = $filename.'.'.$fileUpload['ext'];
		    
		    $this->data['Photo']['file'] = $filename;
		    
		    $this->Photo->create();
		    if ($this->Photo->save($this->data) and $fileUpload) {
			
			$rs1 = copyImage(
				IMAGES.'uploads/'.$filename,
				IMAGES.'uploads/'.$this->Photo->id.'.'.$fileUpload['ext'],
				array('width'=>800,'height'=>500,'resize mode'=>'fill','background'=>array(0,0,0))
				);
			$rs2 = copyImage(
				IMAGES.'uploads/'.$filename,
				IMAGES.'uploads/resized/'.$this->Photo->id.'.'.$fileUpload['ext'],
				array('width'=>100,'height'=>80,'resize mode'=>'crop')
				);
			if (!$rs1 || !$rs2){
			    $this->Photo->delete($this->Photo->id);
			}else{
			    
			    $this->Photo->save(array('id'=>$this->Photo->id,'file'=>$this->Photo->id.'.'.$fileUpload['ext']));
			    $this->Session->setFlash(__('La foto se ha guardado correctamente.', true),'flash/success');
			}
			$this->__deleteFile('img/uploads/'.$filename);
			$this->redirect(array('action' => 'index'));
		    } else {
			 $this->Photo->delete($this->Photo->id);
			$this->Session->setFlash(__('La foto no pudo guardarse.', true),'flash/failure');
		    }
			
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Foto no encontrada', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Photo->save($this->data)) {
				$this->Session->setFlash(__('Los cambios se guardaron correctamente.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La descripción de la foto no pudo cambiarse.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Photo->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Foto no encontrada', true),'flash/failure');
			$this->redirect(array('action'=>'index'));
		}
		
		$photo = $this->Photo->find('first',array('conditions'=>array('id'=>$id),'contain'=>array()));
		if ($this->Photo->delete($id)) {
			
			$this->__deleteFile('img/uploads/'.$photo['Photo']['file']);
			$this->__deleteFile('img/uploads/resized/'.$photo['Photo']['file']);
			
			$this->Session->setFlash(__('Foto eliminada!', true),'flash/success');
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('La foto no pudo eliminarse.', true),'flash/failure');
		$this->redirect(array('action' => 'index'));
	}
}
