<?php
class ServerController extends AppController {
  var $name = 'Server';
  var $uses = '';
  
  function __beforeFilter(){
    
  }
  
  function admin_index(){
      $this->set('title_for_layout',__('Administración',1));
  }
  function admin_state() {
    $this->set('title_for_layout',__('Estado del servidor',1));
  }
  
  function admin_phpinfo() {
  	$this->layout = '';

  }
    
  
  function admin_cache(){
    
    if (!empty($this->data)) {
      $result = clearCache(); 
      $this->Session->setFlash(__('La cache fué eliminada correctamente.',1));
    }
    
   
    $this->set('title_for_layout',__('Control de caché',1));
    
  }
  
  
  
  function admin_logs($file=NULL){
    $logs = array();
    $handle = opendir(LOGS);
    
    while(($fileName=readdir($handle))!==false){
      if($fileName == '.' || $fileName == '..') continue;
      $logs[] = $fileName;
    }
    $this->set('logs',$logs);
    
    if($file){
      $file = file(LOGS.$file);
      $this->set('file',$file);
    }
    
    
    $this->set('title_for_layout',__('Registro de eventos',1));
  }
    
    
}

?>
