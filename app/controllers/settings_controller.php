<?php

class SettingsController extends AppController {

	var $uses = array('Setting');
	var $helpers = array('Text');
	
	function admin_index() {
		$this->Setting->recursive = 0;
		$this->paginate = array('order' => 'group');
		$this->set('settings', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Setting->create();
			if ($this->Setting->save($this->data)) {
				$this->Session->setFlash(__('Nueva variable de configuración guardada correctamente.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Setting could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Setting', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Setting->save($this->data)) {
				$this->Session->setFlash(__('Variable de configuración actualizada correctamente.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Los cambios no pudieron ser guardados, intentelo de nuevo.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Setting->read(null, $id);
		}

	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Variable de configuración erronea.', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Setting->delete($id)) {
			$this->Session->setFlash(__('Variable eliminada, compruebe su código fuente en busca de llamadas.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('La variable de configuración no pudo ser eliminada.', true));
		$this->redirect(array('action' => 'index'));
	}

}

?>