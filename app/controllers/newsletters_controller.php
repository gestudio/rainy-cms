<?php

class NewslettersController extends AppController {

	var $name = 'Newsletters';
	var $helpers = array('Html', 'Form', 'Ajax');
	var $components = array('RequestHandler', 'Email');

	function index() {

		$this->set('title_for_layout',__('Subscripción al boletín de noticias.', 1));


		if (!empty($this->data)) {

			if (preg_match("/^([^@]+)(@.*)$/", $this->data['Newsletter']['email']) and !empty($this->data['Newsletter']['conditions'])) {
				if (!empty($this->data['Newsletter']['name']))
					$this->data['Newsletter']['name'] = ucwords(strtolower($this->data['Newsletter']['name']));
				else
					$this->data['Newsletter']['name'] = Inflector::humanize(strtolower(preg_replace("/^([^@]+)(@.*)$/", "$1", $this->data['Newsletter']['email'])));

				$this->data['Newsletter']['email'] = strtolower($this->data['Newsletter']['email']);

				$this->Newsletter->create();
				if ($this->Newsletter->save($this->data)) {
					$this->Session->setFlash(__('Su alta ha sido tramitada con exito.', true)); //ESTE FLASH DEBERIA SER VERDE
					//Enviar el correo al cliente
					$this->Email->from = Configure::read('SITE.TITLE').' <'.Configure::read('SITE.ADMINISTRATOR').'>';
					$this->Email->to = $this->data['Newsletter']['name'].' <'.$this->data['Newsletter']['email'].'>';
					$this->Email->subject = 'Subscripcion a boletin '.Configure::read('SITE.TITLE');
					$this->Email->template = 'alta_newsletter';
					$this->Email->sendAs = 'both';
					$this->Email->layout = 'newsletter';
					$this->set('email',$this->data['Newsletter']['email']);
					$this->set('id',$this->Newsletter->id);
					$this->Email->attachments = array('logo.png' => IMAGES . 'logo.png');

					$this->Email->send();
					

					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Error interno, no pudo ejecutarse la consulta.', true),'flash_failure');
				}
			} elseif(empty($this->data['Newsletter']['conditions'])) {
				$this->Newsletter->invalidate('conditions', 'Debe aceptar la nota legal');
			} else {
				$this->Newsletter->invalidate('email', 'Dirección incorrecta');
			}
		}
	}



	function admin_index() {
		$this->set('title_for_layout',__('Usuarios subscritos al boletín', 1));
		$this->Newsletter->recursive = 0;
		$this->set('Newsletters', $this->paginate());
	}

	function admin_add() {
	    $this->set('title_for_layout',__('Añadir usuario al boletín', 1));
		if (!empty($this->data)) {
			$this->Newsletter->create();
			$this->data['Newsletter']['name'] = ucwords(strtolower($this->data['Newsletter']['name']));
			if ($this->Newsletter->save($this->data)) {
				$this->Session->setFlash(__('Nuevo usuario guardado correctamente', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Error al guardar, compruebe los datos enviados.', true),'flash_failure');
			}
		}
	}

	function admin_add_masive() {


		$this->set('title_for_layout',__('Añadir usuarios de forma masiva', 1));



		if (!empty($this->data)) {

			$this->data['Newsletter']['emails'] = preg_replace("[^A-Za-z0-9@,;. /-_]", '', $this->data['Newsletter']['emails']);
			$direcciones = preg_split("/[\n\r\,; <>]/", $this->data['Newsletter']['emails']);
			$direcciones = array_unique($direcciones);

			$newAddresses = array();
			$currentName = '';

			foreach ($direcciones as $i => $direccion):
				if (preg_match("/^([^@]+)(@.*)$/", $direccion)) {
					//Es email
					$currentName = trim($currentName);
					if (empty($currentName)) {
						//De la parte anterior al @ sacamos el nombre si no lo teniamos de antes
						$currentName = Inflector::humanize(preg_replace("/^([^@]+)(@.*)$/", "$1", $direccion));
					}
					$newAddresses[$i]['name'] = $currentName;
					$newAddresses[$i]['mail'] = $direccion;
					$currentName = '';
				} else {
					//No es email, es nombre.
					$currentName .= ' ' . Inflector::slug(ucfirst(strtolower($direccion)));
					unset($direcciones[$i]);
				}
			endforeach;

			$direcciones = array_values($newAddresses);
			unset($newAddresses);

			$i = null;
			$direccion = null;
			$resultado = 0;

			$this->loadModel('User');




			foreach ($direcciones as $i => $direccion):

				$query = null;

				$buscar = $this->Newsletter->find('count', array('conditions' => array('email' => $direccion['mail'])));
				$buscar2 = $this->User->find('count', array('conditions' => array('email' => $direccion['mail'])));

				if (!$buscar and !$buscar2) {
					$this->Newsletter->create();
					$this->Newsletter->set('name', trim($direccion['name']));
					$this->Newsletter->set('email', trim($direccion['mail']));
					$query = $this->Newsletter->save();
				}


				if ($query)
					++$resultado;

			endforeach;

			$this->Session->setFlash(String::insert(__('Se han insertado :total direcciones nuevas', 1), array('total' => $resultado)));
			$this->redirect(array('action' => 'admin_index'));
		}
	}



	function admin_delete($id = null) {
	    
	    $this->set('title_for_layout',__('Eliminar usuario', 1));
		if (!$id) {
			$this->Session->setFlash(__('Error falta de ID', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Newsletter->delete($id)) {
			$this->Session->setFlash(__('Usuario eliminado de la lista.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Error, no pudo ejecutarse la consulta, intentelo de nuevo.', true));
		$this->redirect(array('action' => 'index'));
	}

	
	
	
	
	
	
	
	
	
	function admin_newsletter() {
		$this->set('title_for_layout',__('Redactar nuevo boletín', 1));
		
		if (!empty($this->data)) {

			//Obtener destinatarios de $this->data['Newsletter']['group']; 0 Todos, 1 No Registrados, 2 Registrados
			$destinatarios1 = array();
			$destinatarios2 = array();

			if ($this->data['Newsletter']['group'] == 1) {
				$this->loadModel('Newsletter');
				$destinatarios1 = $this->Newsletter->find('all', array('conditions' => array('deleted' => 0), 'fields' => array('name', 'email')));
			} elseif ($this->data['Newsletter']['group'] == 2) {
				$this->loadModel('User');
				$destinatarios2 = $this->User->find('all', array('fields' => 'email', 'contain' => array(), 'conditions' => array('bulletin' => 1)));
			} elseif ($this->data['Newsletter']['group'] == 0) {
				$this->loadModel('Newsletter');
				$this->loadModel('User');
				$destinatarios1 = $this->Newsletter->find('all', array('conditions' => array('deleted' => 0), 'fields' => array('name', 'email')));
				$destinatarios2 = $this->User->find('all', array('fields' => 'email', 'contain' => array(), 'conditions' => array('bulletin' => 1)));
			}else
				die(__('Error interno de parámetros', 1));

			$total = count(array_merge($destinatarios1, $destinatarios2));

			if (isset($this->params['named']['confirmation'])) {

				//Reconvertir array de User a Newsletter antes de enviar newsletter
				if (!empty($destinatarios2)) {
					foreach ($destinatarios2 as $destinatario)
						$destinatarios1[] = array('Newsletter' => array('name' => $destinatario['User']['email'], 'email' => $destinatario['User']['email']));
				}


				//Cargar datos de correo para newsletter
				Configure::load('smtp');

				unset($destinatario);

				//Hay que recorrer los 2 arrays de destinatarios enviandole a cada uno el email y pasando su nombre e email a la vista del correo
				foreach ($destinatarios1 as $destinatario) {

					$this->Email->reset();
					$this->Email->from = Configure::read('SITE.TITLE') . ' <' . Configure::read('SITE.ADMINISTRATOR') . '>';
					$this->Email->to = $destinatario['Newsletter']['name'] . ' <' . $destinatario['Newsletter']['email'] . '>';
					$this->Email->subject = $this->data['Newsletter']['title'];
					$this->Email->sendAs = 'both'; // because we like to send pretty mail
					$this->Email->layout = 'newsletter';
					$this->Email->template = 'newsletter';
					$this->set('email',$destinatario['Newsletter']['email']);
					$this->set('id',$destinatario['Newsletter']['id'] );
					$this->data['Newsletter']['email'] = $destinatario['Newsletter']['email'];

					//give data to email template
					$this->set('data', $this->data['Newsletter']);

					//Email is sent here
					$this->Email->send();

				} //End foreach



				$this->Session->setFlash(String::insert(__('La tarea de envio ha sido iniciada a :number destinatarios', 1), array('number' => $total)));
				$this->redirect(array('action' => 'admin_newsletter'));
			}



			$this->set('total', $total);
			unset($destinatarios1);
			unset($destinatarios2);
			unset($destinatarios);


			///////////////////////////////////////////////////////
			$this->render('admin_newsletter_preview');
		}
		if (isset($this->params['named']['email']))
			$this->data = unserialize($this->params['named']['email']);
		
	}

	
	
	
	
	
	function erase($email, $md5) {
		
		$email = base64_decode($email);

		if (md5($email) == $md5) {

			$this->loadModel('Newsletter');
			$this->loadModel('User');
			$this->Newsletter->updateAll(array('deleted' => 1), array('email' => $email));
			$this->User->updateAll(array('bulletin' => 0), array('email' => $email));

			$this->Session->setFlash(String::insert(__('La dirección :email ha sido eliminada correctamente.', 1), array('email' => $email)), 'flash/success');
		} else {
			$this->Session->setFlash(__('Error interno, no se ejecutó la petición.', 1), 'flash/failure');
		}

		$this->redirect(array('action' => 'index'));
	}

}

?>