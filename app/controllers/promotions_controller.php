<?php
class PromotionsController extends AppController {
  var $name = 'Promotions';
  var $uses = 'Promotion';
  var $helpers = array('Html','Form','Javascript','Cache');
  
  
  
  var $cacheAction = array(
    'index' => array('callbacks' => true, 'duration' => '1 year'),
    'index_slideshow' => array('callbacks' => true, 'duration' => '1 year'),
  );
  
  
 
  
  
  function index() {
    
    $arr_promocion = $this->Promotion->find('all', array(
									'conditions' => array('Promotion.selected' => 1),
                                                        		'order' => 'Promotion.created DESC, Promotion.name'
                                                    ));
    $this->set('promocion'  , $arr_promocion);
  }
  
  
  
  
  
  
  
  
  
  
  function admin_index($action = NULL, $object_id = NULL) {

    $arr_promocion = $this->Promotion->find('all', array(
																'conditions' => array('Promotion.selected' => 1), 
																'order' => array('Promotion.created DESC', 'Promotion.name')
                                                         ));
    $this->set('arr_promocion'  , $arr_promocion);

    $this->set('registros', $this->Promotion->find('all', array(
																'conditions' => array('Promotion.selected' => 0),
																'order' => array('Promotion.created DESC', 'Promotion.name')
                         								)));
    
    if (isset($action)){      
      if ($action == 'borrar') {
          $this->Promotion->delete($object_id);
      } else if ($action == 'seleccionar') {
      	  /*
          if (!empty($promocion)) {
            $this->Promotion->set_selected($promocion['Promotion']['id'], 0);
          }
          */
          $this->Promotion->set_selected($object_id);
      } else if ($action == 'deseleccionar') {
          $this->Promotion->set_selected($object_id, 0);
      }
      
      $this->redirect(array('plugin' => null, 'admin'=>true,'controller'=>'promotions','action'=>'index'));
      
    }
    
    if (!empty($this->data)) {
      $this->data['Promotion']['title'] = $this->data['Promotion']['name'];
      $this->Promotion->save($this->data['Promotion']);
      $this->redirect(array('plugin' => null, 'admin'=>true,'controller'=>'promotions','action'=>'index'));
    }
    
	$this->set('title_for_layout','Promociones');
    
  }
  
  
  function admin_edit($id = 0) {
    //Validate current user for admin privileges
	  $this->checkAuthorized(array(1));
    
    if (empty($this->data)) {
      $this->Promotion->id = $id;
      $this->data = $this->Promotion->read();
    } else {
      $this->Promotion->save($this->data['Promotion']);
      $this->redirect(array('plugin' => null, 'admin'=>true,'controller'=>'promotions','action'=>'edit',$this->data['Promotion']['id']));
    }
    
    $this->set('title_for_layout','Promociones');
  }
  


}
?>