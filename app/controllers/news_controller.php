<?php
class NewsController extends AppController {

	var $name = 'News';
	var $components = array('simplePie'); 
	var $helpers = array('rss','Javascript','Cache');
	var $uses = array();
	
	var $cacheAction = array(
	  'index' => array('callbacks' => true, 'duration' => 3)
	);
  
	//App::import('Sanitize');
	
	//////////////////////////////////////////////////////////
	
	function index() {
		$this->loadModel('RssNews');
		$this->set('title_for_layout', __('Noticias', 1));
		$this->paginate = array(
			'limit' => 16,
			'order' => array(
				'RssNews.pubDate' => 'desc',
			),
		);
		$this->set('news', $this->paginate('RssNews'));
	}
	
	

	function last_news() {
		$this->loadModel('RssNews');
		$this->paginate = array(
			'limit' => 6,
			'order' => array(
				'RssNews.pubDate' => 'desc',
			),
		);
		return $this->paginate('RssNews');
	}
	
	function twitter(){
	
		$this->pageTitle = __('Nuestro Twitter',1);
		$items = $this->simplePie->feed('http://twitter.com/statuses/user_timeline/'.Configure::read('SOCIAL.TWITTER').'.rss');
		
		$this->set(compact('items'));
	}

}
?>