<?php
/*
 * SimplePie CakePHP Component
 */

class simplePieComponent extends Object {
  var $cache;

  function __construct() {
    $this->cache = CACHE . 'rss' . DS;
  }

  function feed($feed_url) {
    
    //make the cache dir if it doesn't exist
    if (!file_exists($this->cache)) {
      $folder = new Folder();
      $folder->mkdir($this->cache); 
    }

    //include the vendor class
    App::import('Vendor', 'simplepie', array('file' => 'simplepie'.DS.'simplepie.inc'));
	
    //setup SimplePie
    $feed = new simplePie();
    $feed->set_feed_url($feed_url);
    $feed->set_cache_location($this->cache);
	
    //retrieve the feed
    $feed->init();
	
    //get the feed items
    $items = $feed->get_items();
	
    //return
    if ($items) {
      return $items;
    } else {
      return false;
    }
  }
}
?>