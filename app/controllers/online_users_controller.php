<?php

class OnlineUsersController extends AppController {
	var $name = 'OnlineUsers';
	var $layout = null; //Añadido en nuevo layout
	var $components = array('Session');

	function view($showData = 0) {

		$invitados = $this->OnlineUser->find('count', array('contain'=>array(),'conditions' => array('user_id' => 0)));
		$miembros = $this->OnlineUser->find('count', array('contain'=>array(),'conditions' => array('user_id <>' => 0)));

		$this->set('invitados', $invitados);
		$this->set('miembros', $miembros);
	}



	/*
	 * Function getLastLogin
	 *
	 * @param $user_id (int)
	 * @description Returns last user_id login 
	 * @return $lastOnline (array)
	 * @access public
	 *
	 */
	function getLastLogin($user_id){

		$this->loadModel('UserLogin');
		$lastOnline = $this->UserLogin->find('first',array('order'=>'created DESC','contain'=>array(),'offset'=>1));
		
		return $lastOnline;
	}





}
?>
