<?php
class EventsController extends AppController {

	var $name = 'Events';
	var $helpers = array('Text');

	function index() {
		$this->Event->recursive = 0;
		$this->paginate = array('order'=>'date','conditions'=>array('date >'=>date('Y-m-d')));
		$this->set('events', $this->paginate());
	}

	
	function admin_index() {
		$this->Event->recursive = 0;
		$this->paginate = array('order'=>'date');
		$this->set('events', $this->paginate());
	}


	function admin_add() {
		if (!empty($this->data)) {
			$this->Event->create();
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('Evento guardado', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El evento no pudo guardarse, intentelo de nuevo.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Evento no encontrado', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('Evento guardado correctamente.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El evento no pudo guardarse, intentelo de nuevo.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Event->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Evento no encontrado.', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Event->delete($id)) {
			$this->Session->setFlash(__('Evento eliminado.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('El evento no pudo ser eliminado.', true));
		$this->redirect(array('action' => 'index'));
	}
}
