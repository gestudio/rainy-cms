<?

class UsersController extends AppController {

	var $components = array('Email', 'Security', 'RequestHandler','Cookie');
	var $uses = array('User', 'UserGroup');
	var $helpers = array('ajax', 'Session');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login', 'logout');
		
	}

	/**
	 * 
	 * Function admin_index
	 * =================================
	 * 
	 * @param	null
	 * @return	null
	 * @description
	 * 	Listado de usuarios con paginate
	 * 
	 * 
	 * */
	function admin_index() {
		$this->set('title_for_layout',__('Listado de contactos',1));

		$this->paginate = array(
			'limit' => 40,
			'order' => array('User.created' => 'DESC'),
			'contain' => array('UserDetail'=>array('name','surname'),'UserGroup' => array('name'))
		);
		$this->set('usersGroups', $this->User->UserGroup->find('list'));

		$this->set('users', $this->paginate('User'));

	}

	/**
	 * 
	 * Function admin_add
	 * =================================
	 * 
	 * 
	 * */
	function admin_add() {
	    $this->set('title_for_layout',__('Añadir nuevo usuario',1));
		if (!empty($this->data)) {

			$this->data['User']['userCode'] = true;
			$this->data['User']['conditions'] = true;
			$this->data['User']['active'] = true;
			$success = $this->User->saveAll($this->data);
			if ($success) {
				$this->Session->setFlash(__('Usuario creado correctamente.', true), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->data['User']['password'] = '';
				$this->data['User']['passwordR'] = '';
				//debug($success);
				//debug($this->User->invalidFields());die;
				$this->Session->setFlash(__('Error al crear el usuario.', true), 'flash/failure');
			}
		}

		$this->set('userGroups', $this->User->UserGroup->find('list'));
	}

	
	/**
	 * 
	 * Function admin_add
	 * =================================
	 * 
	 * @param 	$userId (Int)
	 * 
	 * */
	function admin_edit($userId){
	    $this->set('title_for_layout',__('Editar usuario',1));
	    if (!empty($this->data)) {

			$this->data['User']['userCode'] = true;
			$this->data['User']['conditions'] = true;
			$this->data['User']['active'] = true;
			$success = $this->User->saveAll($this->data);
			if ($success) {
				$this->Session->setFlash(__('Usuario creado correctamente.', true), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->data['User']['password'] = '';
				$this->data['User']['passwordR'] = '';
				//debug($success);
				//debug($this->User->invalidFields());die;
				$this->Session->setFlash(__('Error al crear el usuario.', true), 'flash/failure');
			}
		}else{
		    $this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$userId),'contain'=>array('UserDetail')));
		}

		$this->set('userGroups', $this->User->UserGroup->find('list'));
	}
	
	
	
	
	
	function admin_view($userId){
	    $this->set('title_for_layout',__('Detalles de usuario',1));
	    $user = $this->User->find('first',array('conditions'=>array('User.id'=>$userId),'contain'=>array('UserDetail','UserGroup','Document')));
	    //debug($user);die;
	    $this->set(compact('user'));
	}
	
	
	/**
	 * 
	 * Function admin_delete
	 * =================================
	 * 
	 * @param $userId (Int)
	 * @return null
	 * @description
	 * 	Elimina el usuario enviado, devuelve un Session->flash con el resultado y redirige a la página de referencia.
	 * 
	 * 
	 * */
	function admin_delete($id) {
		if ($this->User->delete($id, true))
			$this->Session->setFlash(__('Usuario eliminado correctamente.', 1), 'flash/success');
		else
			$this->Session->setFlash(__('Error al eliminar el usuario.', 1), 'flash/failure');
		$this->redirect($this->referer());
	}


	////////////////////////////////////////////////////////////////////////////	
	////////////////////// FRONTEND FUNC ///////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	
	/*
	 * Function login()
	 *
	 * @params
	 * $this->data (array)
	 * 
	 * @description
	 * Make the login action for the user, when $this->data post array received, check useremail and password
	 * If credentians are correct, redirect to welcome page according to user level.
	 * If previousUrl is set, redirect to that URL.
	 *
	 * @return
	 * null
	 */

	function login() {
		$this->set('title_for_layout',__('Iniciar sesión',1));

		if (!empty($this->data)) {

			// USER SEARCH
			///////////////////////////////////////////////////////////////////////////////////
			$queryConditions = array('email' => $this->data['User']['email'], 'password' => $this->data['User']['password']);
			$loginFoundUser = $this->User->find('first', array('conditions' => $queryConditions, 'contain' => false));

			$tolerance = 5;
			if (!$this->Session->check('Auth.failedTimes')) {
				$failedTimes = 1;
				$this->Session->write('Auth.failedTimes', '1');
			} else {
				$failedTimes = $this->Session->read('Auth.failedTimes');
			}


			if (!empty($loginFoundUser) and $failedTimes <= $tolerance and $loginFoundUser['User']['active']) {

				$loginAction = $this->Auth->login($this->data);

				if (!empty($this->data['User']['remember_me'])) {
					$cookie = array();
					$cookie['email'] = $this->data['User']['email'];
					$cookie['password'] = $this->data['User']['password'];
					$this->Cookie->write('Auth.User', $cookie, true, '+2 weeks');
					unset($this->data['User']['remember_me']);
				}
				$this->User->id = $this->Auth->user('id');
				$this->User->saveField('last_login', time());

				$this->log('Sucess login ' . $this->data['User']['email'] . ' from ' . $_SERVER['REMOTE_ADDR'], 'security');

				$this->Session->delete('Auth.failedTimes');

				$redirect = $this->Session->check('Auth.redirect');
				if ($redirect) {
					$this->redirect($this->Auth->redirect());
					$this->Session->delete('Auth.redirect');
				} else {
					$this->redirect($this->Auth->loginRedirect);
				}
			} elseif ($failedTimes > $tolerance) {
				$this->log('Too much wrong connection attemps from ' . $_SERVER['REMOTE_ADDR'], 'security');
				$this->Session->write('Auth.failedTimes', ++$failedTimes);
				$this->Session->setFlash(__('Se han realizado demasiados intentos de acceso fallidos desde tu IP', true));
				//$this->controller->redirect($this->MM_redirectLoginFailed, 303, true);
			} elseif (!empty($loginFoundUser) and $loginFoundUser['User']['active'] != 1) {
				$this->Session->setFlash(__('Tu cuenta no ha sido activada aún.', true));
				unset($loginFoundUser);
				$this->redirect('/users/confirmation', 303, true);
			} else {
				$this->log('Wrong login attemp from ' . $_SERVER['REMOTE_ADDR'], 'security');
				$this->Session->write('failedTimes', ++$failedTimes);
				$this->Session->setFlash(__('Usuario y contraseña incorrectos.', true),'flash/failure');
			}
		} elseif (empty($this->data)) {
			$cookie = $this->Cookie->read('Auth.User');
			if (!is_null($cookie)) {
				if ($this->Auth->login($cookie)) {
					// Clear auth message, just in case we use it.
					$this->Session->delete('Message.auth');
					$this->redirect($this->Auth->redirect());
				}
			}
		}

	}

	/*
	 * Function logout()
	 *
	 * Make the logout action for the user
	 *
	 */

	function logout($action = 0) {
	    
		$this->set('title_for_layout',__('Sesión cerrada correctamente.',1));
		$this->Cookie->delete('Auth');
		if ($this->Auth->user('email'))
			$this->redirect($this->Auth->logout(), 301, true);

	}

	/*
	 * Function password_recover()
	 *
	 * Receives form data with email address
	 *
	 * If email is within Users table, reset the password and send to the registered email address.
	 *
	 */

	function password_recover() {
		$this->set('title_for_layout',__('Recupera tu clave de acceso', true));


		//if the form was submited
		if (!empty($this->data)) {

			$condiciones = array('User.email' => $this->data['User']['email']);
			
			$currentUser = $this->User->find('first', array('conditions' => $condiciones,'contain'=>array('UserDetail.name')));

			if (!empty($currentUser)) {

				$currentUser['User']['password'] = $this->User->__randomize_password();
				$currentUser['User']['passwordR'] = $currentUser['User']['password'];

				$currentUser = $this->User->hashPasswords($currentUser);

				$this->data['User']['id'] = $currentUser['User']['id'];
				$this->data['User']['email'] = $currentUser['User']['email'];
				$this->data['User']['password'] = $currentUser['User']['password'];
				$this->data['User']['passwordR'] = $currentUser['User']['passwordR'];

				$saveAction = $this->User->save($this->data);

				if (!$saveAction)
					die("Internal error, no password change ocurred.");
				//Send the email with the new passwd
				
				//$this->Email->delivery = 'debug';
				$this->Email->from = Configure::read('SITE.TITLE') . ' <' . Configure::read('SITE.ADMINISTRATOR') . '>';
				$this->Email->to = $currentUser['UserDetail']['name'] . ' <' . $currentUser['User']['email'] . '>';
				$this->Email->subject = __('Recupera tu clave de acceso', true);
				$this->Email->sendAs = 'both'; // because we like to send pretty mail
				$this->Email->template = 'password_recover';

				//give data to email template
				$this->set('user', $currentUser);

				//finally email the message to the user
				$this->Email->send();
				//debug($this->Session->read('Message.email'));die;
				//Email is sent here

				$this->Session->setFlash(__('La nueva clave ha sido enviada a', true) . ' ' . $this->data['User']['email']);

				$this->render('password_sent');
				
			}else {
				$this->Session->setFlash(__('La dirección de email no esta registrada como afiliado.', true), 'flash/failure');
			}
		}//end of the submited form
	}

	/**
	 * 
	 * Function admin_change_password()
	 * 
	 * @params
	 * 
	 * @description
	 * 
	 * 
	 */
	function admin_change_password($userId=null) {
		$this->set('title_for_layout',__('Cambiar la clave de acceso a un usuario', true));

		// Procesamos el formulario
		if (!empty($this->data)) {

			$success = $this->User->save($this->data);

			if ($success) {
				$this->Session->setFlash(__('Contraseña guardada correctamente.', 1), 'flash/success');
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('Error al guardar la contraseña. Comprueba los campos en rojo', 1), 'flash/failure');
			}
			// Cargamos la info del usuario
		} else {
			// Cargasmo el usuario
			$this->data = $this->User->find('first', array(
						'conditions' => array('User.id' => $userId),
						'fields' => array('id', 'email'),
						'contain' => array()
					));

			// Comprobamos que el id exista
			if (empty($this->data)) {
				$this->Session->setFlash(__('Usuario inexistente', 1), 'flash/failure');
				$this->redirect(array('action' => 'index'));
			}
		}

	}



}