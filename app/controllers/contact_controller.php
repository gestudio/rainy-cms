<?php

class ContactController extends AppController {

	var $name = 'Contact';
	var $components = array('RequestHandler', 'Email');

	function index() {
		$this->pageTitle = __('Contacta con nosotros', 1);
		if ($this->RequestHandler->isPost()) {
			$this->Contact->set($this->data);
			if ($this->Contact->validates() && $this->data['Contact']['lopd'] == true) {
				//send email using the Email component
				$this->Email->to = Configure::read('SITE.ADMINISTRATOR');

				$this->Email->subject = Configure::read('SITE.TITLE') . ' ' . date('H:i Y/m/d', time()) . ' ' . __('Mensaje de contacto', true);
				$this->Email->from = $this->data['Contact']['email'];
				$this->Email->sendAs = 'both'; // because we like to send pretty mail
				$this->Email->template = 'contact';

				$this->Email->send();

				$this->set('name', $this->data['Contact']['name']);
				
				$this->loadModel('Newsletter');
				if(!empty($this->data['Contact']['email']))
					$this->Newsletter->save(array('name'=>$this->data['Contact']['name'],'email'=>$this->data['Contact']['email']));
			} else {
			    if($this->data['Contact']['lopd'] != true) $this->Contact->invalidate('lopd',__('Debes aceptar la LOPD',1));
				$this->set('hasError', 1);
			}
		}
	}

	
	function paqueteria() {

		$this->pageTitle = __('Paquetería a Cuba', 1);
		if ($this->RequestHandler->isPost()) {
			
			
			if (!empty($this->data['sender']['name']) and !empty($this->data['sender']['email']) and !empty($this->data['sender']['phone']) and !empty($this->data['sender']['surname'])) {
				
				//send email using the Email component
				$this->Email->to = Configure::read('SITE.ADMINISTRATOR');

				$this->Email->subject = Configure::read('SITE.TITLE') . ' ' . date('H:i Y/m/d', time()) . ' ' . __('Solicitud de mensajeria o paqueteria', true);
				$this->Email->from = $this->data['sender']['email'];
				$this->Email->sendAs = 'both'; // because we like to send pretty mail
				$this->Email->template = 'paqueteria';

				$this->Email->send();

				$this->set('name', $this->data['sender']['name']);
			} else {
				$this->Session->setFlash('Ocurrió un error al enviar los datos.','flash_failure');
			}
		}
	}
	
	
	
	
	function telefono(){
		
	}
	

	
	
	
	
	
}

?>
