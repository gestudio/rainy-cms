<?php
class BlogController extends AppController {

	var $name = 'Blog';
	var $helpers = array('Text');
	var $components = array('Email');

	function admin_index() {
		$this->Blog->recursive = 0;
		$this->set('blogs', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid blog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('blog', $this->Blog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Blog->create();
			if ($this->Blog->save($this->data)) {
				$this->Session->setFlash(__('The blog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.', true));
			}
		}
		$users = $this->Blog->User->find('list');
		$photos = $this->Blog->Photo->find('list');
		$categories = $this->Blog->Category->find('list');
		$this->set(compact('users', 'photos', 'categories'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid blog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Blog->save($this->data)) {
				$this->Session->setFlash(__('The blog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Blog->read(null, $id);
		}
		$users = $this->Blog->User->find('list');
		$photos = $this->Blog->Photo->find('list');
		$categories = $this->Blog->Category->find('list');
		$this->set(compact('users', 'photos', 'categories'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for blog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Blog->delete($id)) {
			$this->Session->setFlash(__('Blog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Blog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
