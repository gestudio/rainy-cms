<?php

class DocumentsController extends AppController {

    var $name = 'Documents';
    var $helpers = array('Text','Custom');
    var $components = array('Email');

    function index() {

	if(!isset($this->data['User']['email']) || empty($this->data['User']['email'])){
	    $this->render('require_login');
	}else{
	    $this->loadModel('User');
	    
	    $user = $this->User->find('first',array('conditions'=>array('User.email'=>$this->data['User']['email'],'User.password'=>$this->data['User']['password']),'contain'=>array('UserDetail','UserGroup','Document')));
	    
	    if(empty($user)){
		$this->data['User']['password'] = '';
		$this->Session->setFlash(__('No se encontraron documentos.',1),'flash/failure');
		$this->render('require_login');
	    }
	    $this->set(compact('user'));
	}
	
    }

    function admin_index() {

	$this->paginate = array('contain' => array('User' => array('fields' => array('id', 'email'),'UserDetail.full_name'), 'Recipient' => array('fields' => array('email'))));


	$this->Document->recursive = 0;
	$this->set('documents', $this->paginate());
    }

    function admin_add() {
	if (!empty($this->data)) {
	    if ($this->data['Document']['file']['error'] == 0) {


		$this->Document->create();


		if (empty($this->data['Document']['name']))
		    $this->data['Document']['name'] = str_replace('.', ' ', $this->data['Document']['file']['name']);
		if ($this->Document->save($this->data)) {

		    $fileUpload = $this->__upload_file($this->data['Document']['file'], 'files', base64_encode($this->Document->id), 10240000);

		    if ($fileUpload) {

			$this->Document->save(array('id' => $this->Document->id, 'ext' => $fileUpload['ext']));


			if ($this->data['Document']['notify'])
			    $this->notificarDestinatarios($this->Document->id);

			$this->Session->setFlash(__('Archivo guardado correctamente.', true));
			$this->redirect(array('action' => 'index'));
		    }else {

			$this->Document->delete($this->Document->id);
			$this->Session->setFlash(__('El archivo no pudo guardarse por el formato o el tamaño.', true));
		    }
		} else {
		    $this->Session->setFlash(__('Datos incorrectos, compruebe los campos.', true));
		}
	    } else {
		$this->Session->setFlash(__('Ocurrió un error al subir el fichero.', 1));
	    }
	}
	$users = $this->Document->User->UserDetail->find('all', array('fields' => array('user_id','full_name'),'contain'=>array('User'=>array('id','user_group_id','UserGroup.name'))));
	
	$users = Set::combine($users,'{n}.UserDetail.user_id','{n}.UserDetail.full_name', '{n}.User.UserGroup.name');
	
	$this->set(compact('users'));
    }

    
    
    
    function admin_edit($docId = null) {
	if (!empty($this->data)) {
	    if ($this->data['Document']['file']['error'] == 0) {


		$this->Document->create();


		if (empty($this->data['Document']['name']))
		    $this->data['Document']['name'] = str_replace('.', ' ', $this->data['Document']['file']['name']);
		if ($this->Document->save($this->data)) {

			$this->Session->setFlash(__('Cambios guardados correctamente.', true));
			$this->redirect(array('action' => 'index'));
		
		} else {
		    $this->Session->setFlash(__('Datos incorrectos, compruebe los campos.', true),'flash/failure');
		}
	    } else {
		$this->Session->setFlash(__('Ocurrió un error al subir el fichero.', 1),'flash/failure');
	    }
	}
	
	
	
	$this->data = $this->Document->find('first',array('conditions'=>array('Document.id'=>$docId),'contain'=>array('Recipient','User.email')));
	
	
	$users = $this->Document->User->UserDetail->find('all', array('fields' => array('user_id','full_name'),'contain'=>array('User'=>array('id','user_group_id','UserGroup.name'))));
	$users = Set::combine($users,'{n}.UserDetail.user_id','{n}.UserDetail.full_name', '{n}.User.UserGroup.name');
	
	$this->set(compact('users'));
    }

    
    
    
    function admin_delete($id = null) {
	if (!$id) {
	    $this->Session->setFlash(__('Invalid id for document', true));
	    $this->redirect(array('action' => 'index'));
	}

	$objeto = $this->Document->find('first', array('conditions' => array('id' => $id), 'contain' => array()));
	$fichero = 'files' . DS . base64_encode($id) . '.' . $objeto['Document']['ext'];

	if ($this->Document->delete($id)) {
	    $this->loadModel('DocumentsUser');
	    $this->DocumentsUser->deleteAll(array('document_id' => $id));

	    $this->__deleteFile($fichero);

	    $this->Session->setFlash(__('Document deleted', true));
	    $this->redirect(array('action' => 'index'));
	}
	$this->Session->setFlash(__('Document was not deleted', true));
	$this->redirect(array('action' => 'index'));
    }

    
    
    
    
    /*
     * 
     * Function notificarDestinatarios
     * 
     * @params docId (int) Document.id to handle
     * 
     * 
     */
    private function notificarDestinatarios($docId) {

	$document = $this->Document->find('first', array('conditions' => array('Document.id' => $docId), 'contain' => array('User','Recipient'=>array('UserDetail.name'))));
	
	foreach ($document['Recipient'] as $recipient) {
	    $this->Email->reset();
	    
	    $this->Email->to = $recipient['UserDetail']['name'].' <'.$recipient['email'].'>';
	    $this->Email->subject = 'Nuevo documento: '.Configure::read('SITE.TITLE');
	    $this->Email->replyTo = $document['User']['email'];
	    $this->Email->from = $document['User']['email'];
	    $this->Email->template = 'documento'; // note no '.ctp'
	    //Send as 'html', 'text' or 'both' (default is 'text')
	    $this->Email->sendAs = 'both'; // because we like to send pretty mail
	    //ATTACHMENT
	    $this->Email->attachments = array(WWW_ROOT . 'files'.DS.base64_encode($docId).'.'.$document['Document']['ext']);
	    $this->set('document',$document);
	    //Do not pass any args to send()
	    $this->Email->send();
	}
    }

}
