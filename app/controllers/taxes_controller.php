<?php
class TaxesController extends AppController {

	var $name = 'Taxes';

	function admin_index() {
		$this->Tax->recursive = 0;
		$this->set('taxes', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid tax', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('tax', $this->Tax->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Tax->create();
			if ($this->Tax->save($this->data)) {
				$this->Session->setFlash(__('The tax has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tax could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid tax', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Tax->save($this->data)) {
				$this->Session->setFlash(__('The tax has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tax could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Tax->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for tax', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Tax->delete($id)) {
			$this->Session->setFlash(__('Tax deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Tax was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
