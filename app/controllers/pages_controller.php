<?php

class PagesController extends AppController {

	var $name = 'Pages';
	var $helpers = array('Html', 'Session');
	var $uses = array();

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @access public
	 */
	function display() {
		$path = func_get_args();

		$count = count($path);

		if (!$count) {
			$path[] = 'index';
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if ($page == 'frontpage'){
			if($this->RequestHandler->isMobile()){
				$path = array( 'mobile','frontpage');
				$this->layout = 'mobile';
			}
		}
		
		if (!empty($this->params['title_for_layout']))
			$title_for_layout = $this->params['title_for_layout'];
		elseif (!empty($path[$count - 1]) and !empty($this->params['title_for_layout'])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		
		$this->render(implode('/', $path));
	}

}
