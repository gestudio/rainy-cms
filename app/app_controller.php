<?php

/**
 * Application controller
 *
 * This file is the base controller of all other controllers
 *
 * PHP version 5
 *
 * @category Controllers
 * @version  1.0
 */
class AppController extends Controller {

	/**
	 * Components
	 *
	 * @var array
	 * @access public
	 */
	public $components = array(
		'Session',
		'RequestHandler',
			'Security',
			'Auth'
	);
	/**
	 * Helpers
	 *
	 * @var array
	 * @access public
	 */
	public $helpers = array(
		'Html',
		'Form',
		'Session',
		'Js',
	);
	/**
	 * Cache pagination results
	 *
	 * @var boolean
	 * @access public
	 */
	public $usePaginationCache = true;

	/**
	 * beforeFilter
	 *
	 * @return void
	 */
	public function beforeFilter() {
		$this->Security->blackHoleCallback = '__securityError';
		$this->setAuthComponent();
		
		//Obtener los datos de Setting
		$this->loadAppSettings();
		
		if (isset($this->params['admin'])){
			$this->layout = 'admin/admin';
			$this->Auth->user();
			$this->checkAuthorized(array(1,2));
		}

		if ($this->RequestHandler->isAjax()) 
			$this->layout = 'ajax';
		

		if (!isset($this->params['admin']) && Configure::read('SITE.STATUS') == 0 && !$this->Session->check('Auth.User.role_id')) {
			$this->set('title_for_layout', __('Website out of service', true));
			$this->layout = 'maintenance';
			$this->render('maintenance');
		}

		if (isset($this->params['locale'])) {
			Configure::write('Config.language', $this->params['locale']);
		}
	}

	/**
	 * blackHoleCallback for SecurityComponent
	 *
	 * @return void
	 */
	public function __securityError() {
		$this->cakeError('securityError');
	}

	
	
	
	
	
	
	
	
	/**
	 * 
	 * Function setAuthComponent()
	 * 
	 * @params
	 * null
	 * 
	 * @description
	 * Initializes and configures Auth component.
	 * 
	 * @return
	 * NULL
	 * 
	 * @access
	 * PRIVATE
	 * 
	 */
	private function setAuthComponent(){
	 	//Model configuration for data source
	 	////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->userModel = 'User';
		$this->Auth->fields = array('username'=> 'email','password'=>'password');
		//$this->Auth->userScope = array('User.active' => true);
		$this->Auth->authenticate = ClassRegistry::init('User');
		
		//Default action: no auth required
		////////////////////////////////////////////////////////////////////////////////////////////////
		//$this->Auth->authorize = 'controller';
		$this->Auth->allow('*');
		
		$this->Auth->autoRedirect = false;
		
		
		//Login page
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->loginAction = '/users/login';
		
		//Successful login page  for welcome if no Auth.redirect is set in session
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->loginRedirect = '/admin/server';
		
		//Where to go after log out, beeing default login page
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'logout');
		
		//Error messages
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->loginError = __('Usuario y contraseña incorrectos.',true);
		$this->Auth->authError = __('No estas autorizado a visitar esa página.',true);
		
		
		
		//UnAuthorized ajax request will render this element
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->Auth->ajaxLogin  = 'requestLogin';
		//Flas element to show Auth messages
		////////////////////////////////////////////////////////////////////////////////////////////////
		//$this->Auth->flashElement = "flash_failure";
		
	}
	
	
	
	
	
	
	/**
	 * 
	 * Function checkAuthorized()
	 * 
	 * @params
	 * $groups (array) Authorized user groups.
	 * 
	 * @description
	 * Checks that the user is loged in.
	 * If no user, redirect to login page.
	 * 
	 * If there is user logged, checks if it is from the allowed groups.
	 * If not in allowed groups, redirect to to referer page.
	 * 
	 * @return
	 * null
	 * 
	 */
	function checkAuthorized($groups=null){
		
		//$this->Security->requireSecure('*');
		
		if(!$this->Auth->user('id')){
			//The is no auth user
			if (isset($this->params['url']['url'])) {
				$url = $this->params['url']['url'];
				$url = Router::normalize($url);
				$this->Session->write('Auth.redirect', $url);
			}
			
			$this->redirect($this->Auth->loginAction);
			
		}elseif($groups!=null && !in_array($this->Auth->user('user_group_id'),(array)$groups)){
			//There is auth user but not enought privileges
			$this->Session->setFlash($this->Auth->authError);
			$this->redirect($this->referer());
			
		}
	}
	
	
	
	
		/**
	 * 
	 * Function loadAppSettings()
	 * 
	 * @params
	 * null
	 * 
	 * @description
	 * Sets in Configure array general configuration values from database.
	 * 
	 * @return
	 * null
	 * 
	 * 
	 * 
	 */
	private function loadAppSettings(){
		if(!Configure::read('SITE.TITLE')){
			// Cargamos las opciones generales
			$this->loadModel('Setting');
  			$settings_array = $this->Setting->find('all');
	  		foreach($settings_array as $key=>$value){
	  			Configure::write($value['Setting']['group'].'.'.$value['Setting']['key'], $value['Setting']['value']);
	  		}
    		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	# @function __upload_file
	#
	# @param $field        	string        	the name of the file upload form field
	# @param $dirPath    	string        	the relative path to which to store the file (no trailing slash)
	# @newFileName          string		desired file name
	# @param $maxSize   	int             the maximum size of the file
	# @param $allowed    	array        	an array containing all the "allowed" file mime-types, if null all are permited
	#
	# @return mixed        the files' stored path on success, false on failure.
	function __upload_file($field = '', $dirPath = '', $newFileName = NULL, $maxSize = 1000000, $allowed = array()){
		foreach ($field as $key => $val)
			$$key = $val;
		
		$subida = is_uploaded_file($tmp_name);
		
		if ((!$subida) || ($error != 0) || ($size == 0) || ($size > $maxSize))
			return false;

		if ((is_array($allowed)) && (!empty($allowed))){
			if (!in_array($type, $allowed)) 	return false;    // file is not an allowed type
		}
		
		$file = new File($name);
		$nombre = strtolower(basename($name));
		$extension = $file->ext();
		if (!is_null($newFileName)) $name = $newFileName.".". $extension;
		$path = $dirPath . DS . $name;
		
		$file = array('path'=>$path,'ext'=>$extension,'name'=>$name);
		if (move_uploaded_file($tmp_name, WWW_ROOT.$path))
			return $file;
	
		return false;
	}
	
	
	
	
	/*
	 * Function __deleteFile
	 *
	 * @param $ruta (String)
	 * @param $nombre_fichero (String) optional if not specified in $ruta
	 * @description Deletes the requested file
	 * @return $result (bool)
	 * @access PUBLIC
	 *
	 */
	function __deleteFile($ruta, $nombre_fichero = '') {
	    //pr($ruta);
	    $action = @unlink(WWW_ROOT.$ruta.$nombre_fichero);
		    return $action;
	}
	
}

?>