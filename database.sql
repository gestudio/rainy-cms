# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: h0.gestudio.com (MySQL 5.0.77)
# Database: gestudio_cms
# Generation Time: 2012-03-08 15:07:34 +0100
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table billings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `billings`;

CREATE TABLE `billings` (
  `id` int(11) NOT NULL auto_increment,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `slug` varchar(32) NOT NULL,
  `entry` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`,`photo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `created`)
VALUES
	(1,'Pruebas',1318611256);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories_blog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories_blog`;

CREATE TABLE `categories_blog` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `category_id` (`category_id`,`blog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table categories_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories_products`;

CREATE TABLE `categories_products` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `category_id` (`category_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `ext` varchar(6) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` (`id`, `user_id`, `name`, `description`, `ext`, `created`)
VALUES
	(1000038,1001,'unixsec-2 1 pdf','','pdf',1318519807),
	(1000035,1001,'Manual de seguridad Unix','Este manual en pdf es importante','pps',1318519039),
	(1000036,1001,'DocumentaciÃ³n de sistemas operativos','Un documento Word sobre sistemas Unix','docx',1318519085),
	(1000037,1001,'Historia de Unix','','doc',1318519113);

/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table documents_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_users`;

CREATE TABLE `documents_users` (
  `id` int(11) NOT NULL auto_increment,
  `document_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `document_id` (`document_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `documents_users` WRITE;
/*!40000 ALTER TABLE `documents_users` DISABLE KEYS */;

INSERT INTO `documents_users` (`id`, `document_id`, `user_id`)
VALUES
	(43,1000035,1009),
	(44,1000036,1001),
	(45,1000037,1007),
	(42,1000035,1008),
	(61,1000043,1010),
	(46,1000038,1007);

/*!40000 ALTER TABLE `documents_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`id`, `name`, `description`, `date`, `created`)
VALUES
	(4,'Birthday Party','Come to our birthday party next to Okachokee lake in West Palm Beach','2013-02-19',1318417552),
	(5,'SpaceX Falcon 9 Rocket Launch','Today they are launching a Falcon 9 Rocket with a mission to discover why Mars lost its atmosphere.','2013-11-18',1318435459);

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table navigation_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `navigation_menus`;

CREATE TABLE `navigation_menus` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `parent_id` int(10) unsigned NOT NULL,
  `lft` int(10) unsigned default NULL,
  `rght` int(10) unsigned default NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(128) NOT NULL,
  `user_groups` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `parent_id` (`parent_id`,`lft`,`rght`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `navigation_menus` WRITE;
/*!40000 ALTER TABLE `navigation_menus` DISABLE KEYS */;

INSERT INTO `navigation_menus` (`id`, `parent_id`, `lft`, `rght`, `name`, `url`, `user_groups`)
VALUES
	(1,0,1,2,'Events','/events','|0|2|'),
	(2,0,3,4,'Pics','/photos','|0|2|'),
	(9,0,17,18,'Events','/admin/events','|1|'),
	(4,0,7,10,'News','/news','|0|1|2|'),
	(6,0,11,12,'Documents','/documents','|0|'),
	(7,0,13,14,'Extranet','/users/login','|0|'),
	(8,0,15,16,'Contact us','/contact','|0|'),
	(10,0,19,20,'Pics','/admin/photos','|1|'),
	(11,0,21,22,'Documents','/admin/documents','|1|'),
	(12,0,23,24,'Contacts','/admin/users','|1|'),
	(13,0,25,26,'Newsletter','/admin/newsletter','|1|'),
	(14,0,27,28,'Promotions','/admin/promotions','|1|'),
	(15,0,29,30,'Preferences','/admin/settings','|1|'),
	(16,0,31,32,'Log out','/users/logout','|1|2|');

/*!40000 ALTER TABLE `navigation_menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table newsletters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `newsletters`;

CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `email` varchar(90) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `modified` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;

INSERT INTO `newsletters` (`id`, `name`, `email`, `deleted`, `modified`, `created`)
VALUES
	(4,'Gestudio','info@gestudio.com',0,0,1315261533);

/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table online_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `online_users`;

CREATE TABLE `online_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `ip` varchar(32) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(11) NOT NULL auto_increment,
  `description` text NOT NULL,
  `file` varchar(64) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;

INSERT INTO `photos` (`id`, `description`, `file`, `created`)
VALUES
	(21,'En geografÃ­a se define como desierto a la zona terrestre en la cual las precipitaciones casi nunca superan los 250 milÃ­metros al aÃ±o y el terreno es Ã¡rido. El desierto puede ser considerado un ecosistema o un bioma.\r\n\r\nUn desierto es un ecosistema que recibe pocas precipitaciones. Tienen reputaciÃ³n de tener poca vida, pero eso depende de la clase de desierto; en muchos existe vida abundante, la vegetaciÃ³n se adapta a la poca humedad y la fauna usualmente se esconde durante el dÃ­a para preservar humedad. El establecimiento de grupos sociales en los desiertos es complicado y requiere de una importante adaptaciÃ³n a las condiciones extremas que en ellos imperan. Los desiertos forman la zona mÃ¡s extensa de la superficie terrestre: con mÃ¡s de 50 millones de kilÃ³metros cuadrados, ocupan casi un tercio de Ã©sta. De este total, 53% corresponden a desiertos cÃ¡lidos y 47% a desiertos frÃ­os.\r\n\r\nLos procesos de erosiÃ³n son factores importantes en la formaciÃ³n del paisaje desÃ©rtico. SegÃºn el tipo y grado de erosiÃ³n que los vientos y la radiaciÃ³n solar han causado, los desiertos presentan diferentes tipos de suelos: desierto arenoso es aquel que estÃ¡n compuesto principalmente por arena, que por acciÃ³n de los vientos conforma las dunas, desierto pedregoso o rocoso es aquel cuyo terreno estÃ¡ constituido por rocas o guijarros (este tipo de desiertos suele denominarse con la palabra Ã¡rabe hamada).\r\n\r\nLos desiertos pueden contener valiosos depÃ³sitos minerales que fueron formados en el ambiente Ã¡rido, o fueron expuestos por la erosiÃ³n. En las zonas bajas se pueden formar salares. Debido a la sequedad de los desiertos, son lugares ideales para la preservaciÃ³n de artefactos humanos y fÃ³siles.\r\n\r\nTambiÃ©n se define desierto como un lugar despoblado, no habitado por humanos ni apenas por ser vivo alguno. SegÃºn esta definiciÃ³n, tambiÃ©n son desiertos los situados en climas mÃ¡s frÃ­os, como el Ã¡rtico o la tundra.','21.jpg',1318541696),
	(20,'Los pingÃ¼inos (familia Spheniscidae, orden Sphenisciformes) son un grupo de aves marinas, no voladoras, que se distribuyen Ãºnicamente en el Hemisferio Sur, sobre todo en sus altas latitudes. Los primeros europeos en registrar la observaciÃ³n de estas aves fueron miembros de la primera expediciÃ³n de Vasco da Gama1 , que las llamaron pÃ¡jaros niÃ±o o pÃ¡jaros bobos por su andar torpe y erguido y al ser un ave incapaz de volar.[cita requerida] AÃ±os mÃ¡s tarde, cuando los primeros britÃ¡nicos vieron a estos animales, los llamaron penguins (del galÃ©s penwyn, pen = cabeza y qwyn = blanca), que era el nombre que daban al alca gigante del AtlÃ¡ntico norte (Pinguinus impennis).1 Sin embargo y pese a las aparentes similitudes resultado de la convergencia evolutiva, las alcas del Hemisferio Norte no estÃ¡n relacionadas con los pingÃ¼inos. Tras la extinciÃ³n del alca gigante a fines del siglo XIX, el nombre pingÃ¼ino se perpetuÃ³ en las aves de la familia Spheniscidae.','20.jpg',1318541148),
	(22,'El centro de diversidad del gÃ©nero se encuentra en las montaÃ±as Pamir e Hindu Kush en las estepas de KazajistÃ¡n,3 encontrÃ¡ndose distribuido por Oriente Medio, IrÃ¡n y AfganistÃ¡n particularmente, hacia la penÃ­nsula de Anatolia, Europa del Sur y norte de Ãfrica, por el Oeste, y hacia el noreste de China, por el Este.\r\n\r\nLos tulipanes se cultivaron como plantas ornamentales desde principios del siglo XI en Anatolia. Desde esta regiÃ³n e IrÃ¡n provendrÃ­an los bulbos tomados por el toledano Ibn Massal para su cultivo en el Al-Ãndalus, segÃºn documenta la obra Umda del botÃ¡nico Abu-I-Jayr, fechada entre los siglos XI y XII, estudiada por los investigadores Esteban HernÃ¡ndez Bermejo y ExpiraciÃ³n GarcÃ­a4 Con la Edad Moderna, su cultivo se extendiÃ³ hacia los paÃ­ses del norte de Europa, convirtiÃ©ndose en la flor sÃ­mbolo de los PaÃ­ses Bajos y parte inseparable de su paisaje. TambiÃ©n tiene un uso como sÃ­mbolo nacional en TurquÃ­a e IrÃ¡n.','22.jpg',1318541722),
	(23,'Un faro es una torre situada cerca de la costa o junto a ella, aunque en algunas ocasiones se encuentra situado dentro del mar a cierta distancia de la costa, que se ubica en los lugares donde transcurren las rutas de navegaciÃ³n de los barcos y que dispone en su parte superior de una lÃ¡mpara potente, cuya luz se utiliza como guÃ­a.','23.jpg',1318541741),
	(24,'Las medusas (tambiÃ©n llamadas aguamalas, malaguas, aguavivas o lÃ¡grimas de mar) son organismos marinos pertenecientes al filo Cnidaria y al de los CelentÃ©reos; son pelÃ¡gicos, de cuerpo gelatinoso, con forma de campana de la que cuelga un manubrio tubular, con la boca en su extremo inferior, a veces prolongado por largos tentÃ¡culos cargados con cÃ©lulas urticantes llamados cnidocitos. Se caracterizan por su movilidad, y variabilidad mesoglea. Aparecieron hace unos 500 millones de aÃ±os.1\r\n\r\nPara desplazarse por el agua se impulsa por contracciones rÃ­tmicas de todo su cuerpo; toma agua, que ingresa en su cavidad gastrovascular y la expulsa, usÃ¡ndola como \"propulsor\".\r\n\r\nEl concepto de medusa no es taxonÃ³mico sino morfolÃ³gico. Muchos cnidarios tienen una alternancia de generaciones, con pÃ³lipos sÃ©siles que se reproducen asexualmente y medusas pelÃ¡gicas que llevan a cabo la reproducciÃ³n sexual. Solo los antozoos carecen de forma medusa; las otras tres clases de cnidarios (hidrozoos, escifozoos y cubozoos) poseen forma pÃ³lipo y forma medusa; dichas medusas presentan caracterÃ­sticas distintivas en las tres clases, de modo que se puede hablar de hidromedusas, escifomedusas y cubomedusas respectivamente.','24.jpg',1318541798),
	(25,'El koala (o, menos frecuente, coala) (Phascolarctos cinereus) es una especie de marsupial diprotodonto de la familia Phascolarctidae, arborÃ­cola cuyo aspecto recuerda al de un oso de peluche, con hÃ¡bitos tranquilos, parecidos a los de un perezoso.','25.jpg',1318541834),
	(26,'Hydrangea (nombre comÃºn hortensia) es un gÃ©nero que incluye unas 100 especies de plantas de flor nativas del Sur y Este de Asia (desde JapÃ³n a China, los Himalayas e Indonesia) y tambiÃ©n en NorteamÃ©rica y SuramÃ©rica. Se conocen normalmente por su nombre comÃºn de hortensias.','26.jpg',1318541866),
	(27,'Chrysanthemum, el crisantemo, es un gÃ©nero de alrededor de 30 especies, de fanerÃ³gamas perennes en la familia Asteraceae, nativa de Asia y nordeste de Europa. En el mercado de los Estados Unidos de NorteamÃ©rica se la nombra como \"mum\".','27.jpg',1318541892),
	(29,'\r\nTechnics (En idioma japonÃ©s: Tekunikuzu ãƒ†ã‚¯ãƒ‹ã‚¯ã‚¹) es una marca de la empresa japonesa Matsushita Electric Industrial Co, Ltd, una compaÃ±Ã­a que produce una variedad de productos electrÃ³nicos.\r\n\r\nBajo la marca Technics, la empresa produce una gran variedad de productos de alta fidelidad, como tocadiscos, amplificadores, receptores, reproductores de cintas, reproductores de CD y altavoces, a la venta en varios paÃ­ses. Se concibiÃ³ originalmente como una lÃ­nea de equipos de audio de alta calidad para competir contra compaÃ±Ã­as como Nakamichi, pero la mayorÃ­a de sus electrodomÃ©sticos fueron renombrados como Panasonic a partir de 2002 (excepto en JapÃ³n, donde la marca es aÃºn popular). Equipos para DJ, pianos electrÃ³nicos y microsistemas de alta fidelidad son algunas productos Technics, que actualmente se venden en los EE.UU. y Europa.','29.jpg',1318580058),
	(30,'Henry Ford (30 de julio de 1863 â€“ 7 de abril de 1947) fue el fundador de la compaÃ±Ã­a Ford Motor Company y padre de las cadenas de producciÃ³n modernas utilizadas para la producciÃ³n en masa.\r\n\r\nLa introducciÃ³n del Ford T en el mercado automovilÃ­stico revolucionÃ³ el transporte y la industria en Estados Unidos. Fue un inventor prolÃ­fico que obtuvo 161 patentes registradas en ese paÃ­s. Como Ãºnico propietario de la compaÃ±Ã­a Ford, se convirtiÃ³ en una de las personas mÃ¡s conocidas y mÃ¡s ricas del mundo.\r\n\r\nA Ã©l se le atribuye el fordismo, sistema que se desarrollÃ³ entre fines de los aÃ±os treinta y principios de los setenta y que creÃ³ mediante la fabricaciÃ³n de un gran nÃºmero de automÃ³viles de bajo coste mediante la producciÃ³n en cadena. Este sistema llevaba aparejada la utilizaciÃ³n de maquinaria especializada y un nÃºmero elevado de trabajadores en plantilla con salarios elevados.\r\n\r\nSi bien Ford tenÃ­a una educaciÃ³n bastante pobre, tenÃ­a una visiÃ³n global, con el consumismo como llave de la paz. Su intenso compromiso de reducciÃ³n de costes llevÃ³ a una gran cantidad de inventos tÃ©cnicos y de negocio, incluyendo un sistema de franquicias que estableciÃ³ un concesionario en cada ciudad de EE. UU. y CanadÃ¡ y en las principales ciudades de seis continentes.\r\n\r\nFord legÃ³ gran parte de su inmensa fortuna a la FundaciÃ³n Ford, pero tambiÃ©n se asegurÃ³ de que su familia controlase la compaÃ±Ã­a permanentemente.','30.jpg',1318580139),
	(31,'El Boeing 737 (pronunciado Â«siete-tres-sieteÂ») es un aviÃ³n de reacciÃ³n de pasajeros de fuselaje estrecho de corto a medio alcance, fabricado por la compaÃ±Ã­a estadounidense Boeing Commercial Airplanes. El 737 se desarrollÃ³ como una versiÃ³n derivada de los Boeing 707 y 727, de menor coste, menor tamaÃ±o y bimotor. Del 737 se han fabricado nueve variantes distintas desde su inicio en 1967, estando la -600, -700, -800 y -900 todavÃ­a en producciÃ³n.\r\n\r\nEl 737 se empezÃ³ a diseÃ±ar en el aÃ±o 1964, realizando su primer vuelo en 1967,2 y entrÃ³ en servicio el 10 de febrero de 1968 con Lufthansa.2 3\r\n\r\nEl 737 ha sido fabricado sin interrupciÃ³n por Boeing desde 1967 con un total de 6.348 aeronaves entregadas y 2.061 aeronaves pendientes de ser fabricadas.4','31.jpg',1318580156),
	(32,'La Iglesia catÃ³lica apostÃ³lica ortodoxa es una comunidad cristiana, cuya antigÃ¼edad, tradicionalmente, se remonta a JesÃºs y a los doce apÃ³stoles, a travÃ©s de una ininterrumpida sucesiÃ³n apostÃ³lica. Es la tercera de las tres grandes iglesias o comunidades cristianas, despuÃ©s de la Iglesia catÃ³lica apostÃ³lica romana y el conjunto de iglesias protestantes, y cuenta con mÃ¡s de 225 millones de fieles en todo el mundo.1 2\r\n\r\nLa Iglesia ortodoxa se considera la heredera de todas las comunidades cristianas de la mitad oriental del MediterrÃ¡neo (esto lleva a ciertas tensiones con iglesias orientales unidas a Roma). Su doctrina teolÃ³gica se estableciÃ³ en una serie de concilios, de los cuales los mÃ¡s importantes son los primeros Siete Concilios, llamados \"ecumÃ©nicos\", que tuvieron lugar entre los siglos IV y VIII. Tras varios desencuentros y conflictos, la Iglesia catÃ³lica ortodoxa y la Iglesia catÃ³lica romana se separaron en el llamado \"Cisma de Oriente y Occidente\", el 16 de julio de 1054. El cristianismo ortodoxo se difundiÃ³ por Europa oriental gracias al prestigio del Imperio bizantino y a la labor de numerosos grupos misioneros.\r\n\r\nLa Iglesia ortodoxa estÃ¡ en realidad constituida por 15 iglesias autocÃ©falas, que sÃ³lo reconocen el poder de su propia autoridad jerÃ¡rquica; por ejemplo, del Patriarca de AlejandrÃ­a, de AntioquÃ­a, de Constantinopla, etc.','32.JPG',1318580507),
	(33,'El perro, cuyo nombre cientÃ­fico es Canis lupus familiaris,1 2 3 es un mamÃ­fero carnÃ­voro domÃ©stico de la familia de los cÃ¡nidos, que constituye una subespecie del lobo (Canis lupus). No obstante, su alimentaciÃ³n se ha modificado notablemente debido principalmente al estrecho lazo que existe con el hombre, hasta el punto en que hoy en dÃ­a sea alimentado usualmente como si fuese un omnÃ­voro. Su tamaÃ±o o talla, su forma y pelaje es muy diverso segÃºn la raza de perro. Posee un oÃ­do y olfato muy desarrollados, siendo este Ãºltimo su principal Ã³rgano sensorial. En las razas pequeÃ±as puede alcanzar una longevidad de cerca de 20 aÃ±os, con atenciÃ³n esmerada por parte del propietario, de otra forma su vida en promedio es alrededor de los 15 aÃ±os.','33.jpg',1318580525),
	(34,'El F/A-18 Hornet (â€˜avispÃ³nâ€™ en inglÃ©s) es un caza polivalente bimotor de cuarta generaciÃ³n de origen estadounidense con capacidad todo tiempo, para ser embarcado en portaaviones. Fue desarrollado en los aÃ±os 1970 por la compaÃ±Ã­a McDonnell Douglas (desde 1997 integrada en Boeing) a partir del prototipo Northrop YF-17 para la Armada de los Estados Unidos (USN o U.S. Navy) y el Cuerpo de Marines estadounidense (USMC). El Hornet tambiÃ©n ha sido exportado a siete paÃ­ses para servir en sus fuerzas aÃ©reas. Desde 1986 es usado para acrobacia aÃ©rea por el grupo de demostraciÃ³n aÃ©rea Blue Angels de la U.S. Navy.\r\n\r\nEste aviÃ³n de combate fue el primero de Estados Unidos en combinar capacidades de caza y ataque para atacar objetivos tanto aÃ©reos como terrestres; de ahÃ­ su designaciÃ³n F/A, \"F\" de Fighter y \"A\" de Attack (en espaÃ±ol: Â«cazaÂ» y Â«ataqueÂ»). Las principales misiones que puede desempeÃ±ar son: caza de escolta, defensa aÃ©rea, supresiÃ³n de defensas aÃ©reas enemigas, interdicciÃ³n, apoyo aÃ©reo cercano y reconocimiento. Por su versatilidad y fiabilidad ha demostrado ser un valioso aparato, aun cuando habÃ­a sido criticado por su falta de alcance y de capacidad para armamento comparÃ¡ndolo con sus predecesores mÃ¡s recientes, como el F-14 Tomcat en el papel de caza y cazabombardero, y los A-6 Intruder y A-7 Corsair II en el papel de ataque.3\r\n\r\nEl F/A-18 se ha fabricado en cuatro versiones principales, primero los F/A-18A/B y posteriormente los mejorados F/A-18C/D. Las versiones A/C son monoplazas mientras que las B/D son biplazas. Las versiones C/D del Hornet han servido como lÃ­nea base de diseÃ±o para la creaciÃ³n del F/A-18E/F Super Hornet, una evoluciÃ³n mÃ¡s grande y avanzada. Comparado con el Hornet original, el Super Hornet es mÃ¡s grande, mÃ¡s pesado, con mÃ¡s alcance y dispone de mÃ¡s capacidad para armamento. La introducciÃ³n del F/A-18E/F ha permitido la retirada de varios tipos de aviones anticuados (como caza, ataque y aviÃ³n cisterna) en la Armada estadounidense, que ahora cuenta con los F/A-18 Hornet y Super Hornet en su flota y, gracias a su polivalencia, cumplen todas las misiones de los modelos retirados.','34.jpg',1318580556),
	(35,'Mujer (del latÃ­n mulier, -eris) es la persona del sexo femenino. Mujer tambiÃ©n remite a distinciones de gÃ©nero de carÃ¡cter cultural y social atribuidas a la mujer asÃ­ como a las diferencias sexuales y biolÃ³gicas de la hembra en la especie humana frente al macho. Mujer hace referencia a lo femenino y en el aspecto reivindicativo a la igualdad de derechos defendida por el feminismo.','35.jpg',1318580627),
	(36,'La troposfera o tropÃ³sfera1 es la capa de la atmÃ³sfera que estÃ¡ en contacto con la superficie de la Tierra.\r\n\r\nTiene alrededor de 17 km de espesor en el ecuador terrestre, y en ella ocurren todos los fenÃ³menos meteorolÃ³gicos que influyen en los seres vivos, como los vientos, la lluvia y los huracanes. AdemÃ¡s, concentra la mayor parte del oxÃ­geno y del vapor de agua. En particular este Ãºltimo actÃºa como un regulador tÃ©rmico del planeta; sin Ã©l, las diferencias tÃ©rmicas entre el dÃ­a y la noche serÃ­an tan grandes que no podrÃ­amos sobrevivir. Es de vital importancia para los seres vivos. La tropÃ³sfera es una de las capas mÃ¡s finas del conjunto de las capas de la atmÃ³sfera.\r\n\r\nLa temperatura en la troposfera desciende a razÃ³n de aproximadamente 6,5 ÂºC por kilÃ³metro de altura.','36.jpg',1318580658),
	(37,'El AIM-9 Sidewinder es un misil de corto alcance, termoguiado, montado en aviones caza para ataques aire-aire. Desde hace poco, tambiÃ©n se puede instalar en helicÃ³pteros de ataque.','37.jpg',1318580671),
	(38,'A principios de 2009, el juez de la Audiencia Nacional Baltasar GarzÃ³n imputÃ³ a Francisco Camps como supuesto implicado en la trama de corrupciÃ³n del denominado caso GÃ¼rtel, de acuerdo con el informe de la FiscalÃ­a anticorrupciÃ³n,10 Al ser Camps aforado como parlamentario autonÃ³mico, el juez GarzÃ³n se vio obligado a inhibirse en favor del Tribunal Superior de Justicia de la Comunidad Valenciana (TSJCV) conforme a lo dispuesto en la Ley de Enjuiciamiento Criminal espaÃ±ola.11\r\n\r\nEl 15 de mayo de 2009 Camps fue nuevamente imputado en el caso GÃ¼rtel â€”en esta ocasiÃ³n por el TSJCVâ€” por la comisiÃ³n de un delito de cohecho por una supuesta aceptaciÃ³n de dÃ¡diva (la aceptaciÃ³n de unos trajes de marca valorados en 12 mil euros).12\r\n\r\nTras una primera declaraciÃ³n, cinco dÃ­as mÃ¡s tarde, el 20 de mayo el TSJCV decidiÃ³ mantener las imputaciones contra Camps,13 y tres meses despuÃ©s el propio TSJCV archivarÃ­a la causa, acordando su sobreseimiento libre.14 No obstante, la ediciÃ³n digital del diario El PaÃ­s del 26 de septiembre alude al pleno conocimiento de la trama por parte de Francisco Camps, exhibiendo unos datos contenidos en unos informes policiales que el TSJCV se negÃ³ a valorar como prueba.','38.jpg',1318580766),
	(53,'Los balÃ©nidos (Balaenidae) son una familia de cetÃ¡ceos misticetos que incluye a los gÃ©neros: Balaena y Eubalaena. El tÃ©rmino ballena es comÃºnmente usado para referirse a todos los misticetos, aunque en sentido estricto se reserva sÃ³lo a los integrantes de esta familia.','53.jpg',1318581467),
	(40,'La aurora polar (o \"aurora polaris\") es un fenÃ³meno en forma de brillo o luminiscencia que aparece en el cielo nocturno, usualmente en zonas polares, aunque puede aparecer en otras partes del mundo por cortos periodos de tiempo. En el hemisferio norte se conoce como aurora boreal, y en el hemisferio sur como aurora austral, cuyo nombre proviene de Aurora, la diosa romana del amanecer, y de la palabra griega BÃ³reas, que significa norte, debido a que en Europa comÃºnmente aparece en el horizonte de un tono rojizo como si el sol emergiera de una direcciÃ³n inusual.\r\n\r\nLa aurora boreal es visible de octubre a marzo, aunque en ciertas ocasiones hace su apariciÃ³n durante el transcurso de otros meses, siempre y cuando la temperatura atmosfÃ©rica sea lo suficientemente baja. Los mejores meses para verla son enero y febrero, ya que es en estos meses donde las temperaturas son mÃ¡s bajas. Su equivalente en latitud sur, aurora austral, posee propiedades similares.','40.jpg',1318580908),
	(41,'El fuego de San Telmo o Santelmo es un meteoro Ã­gneo consistente en una descarga de efecto corona electroluminiscente provocada por la ionizaciÃ³n del aire dentro del fuerte campo elÃ©ctrico que originan las tormentas elÃ©ctricas. Aunque se le llama Â«fuegoÂ», es en realidad un plasma de baja densidad y relativamente baja temperatura provocado por una enorme diferencia de potencial elÃ©ctrico atmosfÃ©rica que sobrepasa el valor de ruptura dielÃ©ctrica del aire, en torno a 3 MV/m.\r\n\r\nEste fenÃ³meno toma su nombre de San Erasmo de Formia (Sanct\' Elmo), patrÃ³n de los marineros, quienes habÃ­an observado el fenÃ³meno desde la antigÃ¼edad y creÃ­an que su apariciÃ³n era de mal agÃ¼ero.\r\n\r\nFÃ­sicamente, es un resplandor brillante blanco-azulado, que en algunas circunstancias tiene aspecto de fuego, a menudo en dobles o triples chorros surgiendo de estructuras altas y puntiagudas como mÃ¡stiles, pinÃ¡culos y chimeneas.\r\n\r\nEl fuego de San Telmo se observa con frecuencia en los mÃ¡stiles de los barcos durante las tormentas elÃ©ctricas en el mar, donde en tales circunstancias tambiÃ©n era alterada la brÃºjula, para mayor desasosiego de la tripulaciÃ³n. Benjamin Franklin observÃ³ correctamente en 1749 que es de naturaleza elÃ©ctrica. TambiÃ©n se da en los aviones y dirigibles. En estos Ãºltimos era muy peligroso ya que muchos de ellos se cargaban con hidrÃ³geno, gas muy inflamable, y podÃ­an incendiarse, tal como ocurriÃ³ en 1937 con el dirigible Hindenburg.\r\n\r\nSe cuenta que el fuego de San Telmo tambiÃ©n puede aparecer en las puntas de los cuernos del ganado durante las tormentas elÃ©ctricas y en los objetos afilados en mitad de un tornado, pero no es el mismo fenÃ³meno que el rayo globular, aunque pueden estar relacionados.\r\n\r\nEn la Grecia antigua, la apariciÃ³n de un Ãºnico fuego de San Telmo se llamaba Â«HelenaÂ» y cuando eran dos se les llamaba Â«CÃ¡stor y PÃ³luxÂ».','41.jpg',1318580967),
	(42,'El buceo es el acto por medio del cual el ser humano se sumerge en cuerpos de agua, ya sea el mar, un lago, un rÃ­o, una cantera inundada o una piscina, con el fin de desarrollar una actividad profesional, recreativa, de investigaciÃ³n cientÃ­fica o militar con o sin ayuda de equipos especiales. Al buceo tradicional (sin aparatos de respiraciÃ³n) se le llama sencillamente buceo, aunque a su modalidad deportiva se le llama apnea o buceo libre. El tÃ©rmino submarinismo define con exactidud la prÃ¡ctica del buceo en el mar, que es ademÃ¡s, y con creces, el buceo mÃ¡s practicado en todo el mundo. Al buceo practicado en cuevas o galerÃ­as inundadas de minas se le llama espeleobuceo y al buceo en lagos de montaÃ±a buceo de altura.\r\n\r\nEn casi todas las modalidades que recurren a aparatos de respiraciÃ³n el sistema mÃ¡s utilizado es el de un regulador alimentado por una o mÃ¡s botellas de aire comprimido. La tecnologÃ­a del regulador permite reducir la alta presiÃ³n de una reserva de aire comprimido a la presiÃ³n del agua que rodea al buceador, de modo que Ã©ste pueda respirar con normalidad y con independencia de cables y tubos de suministro de aire desde la superficie. En 1943 los franceses Jacques-Yves Cousteau y Ã‰mile Gagnan fueron los inventores de los reguladores utilizados todavÃ­a actualmente en el buceo autÃ³nomo (tanto profesional como recreativo). Otros dispositivos de buceo autÃ³nomo ya habÃ­an sido experimentados anteriormente (regulador de ThÃ©odore Guillaumet de 1838,1 regulador Rouquayrol-Denayrouze de 1864, manoregulador de Yves Le Prieur de 1926, regulador de RenÃ© y Georges Commheines de 1937 y 1942, reciclador de aire SCUBA de Christian Lambertsen de 1944)2 3 pero ha sido el regulador de tipo Cousteau-Gagnan el que se ha impuesto hasta nuestros dÃ­as, principalmente por la sencillez y fiabilidad de su mecanismo asÃ­ como por su ligereza y facilidad de transporte durante las inmersiones.','42.jpg',1318581035),
	(43,'El skateboarding o monopatinaje es un deporte callejero que se practica con un skateboard, preferentemente en una superficie plana, en cualquier donde se pueda rodar, ya sea en piscinas, escaleras o simplemente en la calle, aunque tambiÃ©n se puede patinar en un skatepark. El Ãºnico impedimento de este deporte es todo lo que pueda desestabilizar al patinador, como por ejemplo baches en un calle, tambiÃ©n por pequeÃ±as rocas que traba el truck y desestabilizan al patinador\r\n\r\nEn Ã©l, el objetivo es buscar la belleza al manejarlo; no es un deporte que estÃ© directamente vinculado a algÃºn tipo de competiciÃ³n, por lo tanto, se podrÃ­a denominar como libre. Existen diversos trucos realizables sobre el skate, que varÃ­an en dificultad. Los trucos de desliz de tabla (la madera) son llamados slides, como el noseslide, boardslide, por una determinada superficie; trucos de estilo libre (freestyle) (cada persona muestra trucos originales y, generalmente, complejos), trucos de rampa, los que se realizan Ãºnicamente en dichas instalaciones; los denominados simplemente por truco (trick), que consisten en que el skate se despegue de los pies y gire dibujando una determinada figura por el aire para luego volver a la posiciÃ³n correcta para el desplazamiento, como el flip, el heel flip, varials o tambiÃ©n las figuras en que la tabla gira simultÃ¡neamente con todo el cuerpo; y por Ãºltimo, los manuals, diversas maniobras en que el individuo se desplaza sobre el skate sobre una o dos ruedas y puede ser con uno o dos pies (con un pie:one foot manual/con dos pies:manual) si se realiza con las ruedas delanteras se llama nose maunal y con las llantas de atrÃ¡s es manual.\r\n\r\n    S.K.A.T.E: Consiste en que 2 o mÃ¡s skaters realizan trucos por orden. Se trata de copiar el truco que el otro skater ponga. cuando alguien falla, pone truco nuevo otro skater. No vale repetir trucos y tampoco estÃ¡n permitidos los \"no complies\".\r\n\r\n    JAM: Cada skater tiene 45 segundos para exhibir sus mejores trucos en un skatepark reduido, con algunos trucos, gaps, grinds y quarters. El jurado, normalmente constituido por PRO\'s daran puntos, gana el que haga la mejor serie con los mejores trucos. Si te caes, te quitan puntos. Para ganar se necesita la perfeccion en tu ronda.\r\n\r\nHay dos tipos de skate: El street o estilo callejero, creado por Arturo Budulge , y el vertical, que se practica en rampas tambiÃ©n denominado vert.','43.jpg',1318581087),
	(54,'Los anuros (Anura, gr. a(n), \"no\" y ourÃ¡, \"cola\") son un clado de anfibios, con rango taxonÃ³mico de orden, conocidos vulgarmente como ranas y sapos. Se caracterizan por carecer de cola, por presentar un cuerpo corto y muy ensanchado, y por las patas posteriores mÃ¡s desarrolladas y adaptadas para el salto. Los anuros son el grupo mÃ¡s numeroso de anfibios; se estima que existen mÃ¡s de 5.000 especies, repartidas en 48 familias.1 La mayorÃ­a pasa su vida dentro o cerca del agua. Su tamaÃ±o puede variar desde un par de milÃ­metros, como es el caso de las especies del gÃ©nero Eleutherodactylus,2 hasta tallas que superan los 30 centÃ­metros, destacando la rana goliat, el anuro mÃ¡s grande del mundo','54.jpg',1318581507);

/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL auto_increment,
  `billing_id` int(11) default NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `tax_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_category_id` (`tax_id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table promotions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promotions`;

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) character set latin1 collate latin1_spanish_ci NOT NULL default '',
  `show_title` int(1) NOT NULL default '1',
  `active` int(1) NOT NULL default '1',
  `selected` int(1) NOT NULL default '0',
  `image` varchar(90) character set latin1 collate latin1_spanish_ci NOT NULL default '',
  `url` varchar(90) character set latin1 collate latin1_spanish_ci NOT NULL default '',
  `html` longtext character set latin1 collate latin1_spanish_ci NOT NULL,
  `flash_file` varchar(90) character set latin1 collate latin1_spanish_ci NOT NULL default '',
  `flash_width` int(11) NOT NULL default '0',
  `flash_height` int(11) NOT NULL default '0',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `promotions` WRITE;
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;

INSERT INTO `promotions` (`id`, `name`, `show_title`, `active`, `selected`, `image`, `url`, `html`, `flash_file`, `flash_width`, `flash_height`, `created`, `modified`)
VALUES
	(4,'We boost your business',1,1,0,'/img/promotions/0000000004.jpg','','','',0,0,'2011-10-11 20:38:48','2011-10-19 19:44:50'),
	(5,'Dinamic Cities',1,1,0,'/img/promotions/0000000005.jpg','','','',0,0,'2011-10-11 20:45:28','2011-10-19 19:43:24'),
	(6,'Taking care of business people',1,1,0,'/img/promotions/0000000006.jpg','','','',0,0,'2011-10-11 20:46:05','2011-10-19 19:43:22'),
	(7,'Creating new software',1,1,0,'/img/promotions/0000000007.jpg','','','',0,0,'2011-10-11 20:46:31','2011-10-11 20:46:43'),
	(8,'Preserve the history',1,1,0,'/img/promotions/0000000008.jpg','','','',0,0,'2011-10-11 20:46:55','2011-10-19 19:43:20'),
	(9,'We create airworthy systems',1,1,1,'/img/promotions/0000000009.jpg','','','',0,0,'2011-10-19 19:43:46','2011-10-19 19:47:59'),
	(10,'Web apps, email and servers',1,1,1,'/img/promotions/0000000010.jpg','','','',0,0,'2011-10-19 19:57:55','2011-10-19 20:33:11'),
	(11,'Solutions for your needs',1,1,1,'/img/promotions/0000000011.jpg','','','',0,0,'2011-10-19 20:33:56','2011-10-19 20:34:16');

/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` tinyint(3) unsigned NOT NULL auto_increment COMMENT 'id de la geozona donde se ubica la tienda.',
  `group` varchar(32) collate utf8_unicode_ci NOT NULL,
  `key` varchar(64) collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci,
  `value` text collate utf8_unicode_ci,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `group`, `key`, `description`, `value`)
VALUES
	(1,'SITE','STATUS','','1'),
	(2,'SITE','TITLE','','Gestudio.com'),
	(3,'SITE','ADMINISTRATOR','Email del administrador que recibirÃ¡ las notificaciones de contacto.','soporte@gestudio.com'),
	(4,'SITE','PHONE','Telefono para los clientes.','512 700 233'),
	(5,'SITE','TAGLINE','DescripciÃ³n comercial para buscadores.','DiseÃ±amos webs, sistemas de gestiÃ³n de datos y marketing en Internet.'),
	(11,'SITE','SLOGAN','Slogan para el subtitulo de la pÃ¡gina principal.','En Gestudio desarrollamos sistemas de gestiÃ³n online y comercio electrÃ³nico.'),
	(6,'SITE','LANG','','es-ES'),
	(12,'SECURITY','PROTOCOL','Define como deberÃ­a ser redireccionado el usuario al acceder a informaciÃ³n crÃ­tica. Puede ser HTTP o HTTPS.','http'),
	(8,'SITE','KEYWORDS','Palabras clave para Buscadores','diseÃ±o,web,gestion,online,cms,hosting,dominios,catalogo,tienda,venta,email,corporativo,empresa,base,datos,emailing,marketing,sms,servidores,linux,servers,bilbo,bilbao,directorio'),
	(9,'SOCIAL','TWITTER','Nombre de usuario de Twitter','gestudio_com'),
	(10,'SOCIAL','FACEBOOK','Nombre de pÃ¡gina de Facebook.','gestudio');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table taxes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taxes`;

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL,
  `value` tinyint(11) NOT NULL COMMENT 'Tax percentage value',
  `modified` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `taxes` WRITE;
/*!40000 ALTER TABLE `taxes` DISABLE KEYS */;

INSERT INTO `taxes` (`id`, `name`, `value`, `modified`)
VALUES
	(1,'I.V.A 18%',18,1318613913);

/*!40000 ALTER TABLE `taxes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) default NULL,
  `dni` varchar(12) default NULL,
  `company` varchar(64) default NULL,
  `address` text,
  `city` varchar(64) default NULL,
  `pc` int(6) default NULL,
  `province` varchar(64) default NULL,
  `phone` int(12) default NULL,
  `mobile` int(12) default NULL,
  `remarks` text,
  `modified` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Detalles de cada usuario';

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;

INSERT INTO `user_details` (`id`, `user_id`, `name`, `surname`, `dni`, `company`, `address`, `city`, `pc`, `province`, `phone`, `mobile`, `remarks`, `modified`)
VALUES
	(1,1001,'Soporte','Gestudio','','','','West Palm Beach',39700,'',NULL,NULL,'',1318524899);

/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) character set latin1 NOT NULL,
  `created` int(11) NOT NULL default '0',
  `modified` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'Administradores',1277707013,1277707013),
	(6,'Clientes',1318417737,1318417737);

/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `user_group_id` int(1) NOT NULL,
  `email` varchar(50) collate utf8_unicode_ci NOT NULL,
  `password` varchar(45) collate utf8_unicode_ci NOT NULL,
  `blacklisted` tinyint(1) NOT NULL default '0',
  `bulletin` tinyint(1) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '0',
  `last_login` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_group_id` (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `user_group_id`, `email`, `password`, `blacklisted`, `bulletin`, `active`, `last_login`, `created`)
VALUES
	(1001,1,'user@isp.tld','',0,0,1,1321130635,1315145242);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
